﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Antlr.Runtime;

namespace CECS543MetricsSuite
{
    public partial class FrmMain : Form
    {
        // keep track of current page
        private TabPage currentTabPage;
        public bool changedBit;

        // Reference to save file
        private SaveFile saveFile = new SaveFile();
        public SaveFile FileSave
        {
            get { return saveFile; }
            set { saveFile = value; }
        }

        int saveFileIndex = 0; // stores the tab index
        private SaveMetrics saveObject; // store save object for writing to file

        public SaveMetrics SaveObject
        {
            get { return saveObject; }
            set { saveObject = value; }
        }

        public Language SelectedLanguage { get; private set; }

        public FrmMain()
        {
            InitializeComponent();
            this.mnuOpen.Click += new System.EventHandler(this.mnuOpen_Click);
            this.mnuClose.Click += new System.EventHandler(this.mnuClose_Click);
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
        }

        /*Part of right click menu for tree view nodes that keeps track of the tabs.*/
        private void mnuDelete_Click(object sender, EventArgs e)
        {
            TreeNode currentNode = tvwMetrics.SelectedNode;
            int currentNodeIndex = currentNode.Index;
            DialogResult dr = MessageBox.Show("Are you sure you want to delete this panel?", "Warning",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dr == DialogResult.Yes)
            {
                // remove from hash tables deleted panel.
                treeViewToTabs.Remove(currentNode);
                tabsToTreeView.Remove(currentTabPage);
                // make sure to not remove page again from tab if already closed.
                if (!closedTabPages.ContainsKey(currentNodeIndex))
                {
                    tabsMetrics.TabPages.Remove(currentTabPage);
                }
                else
                {
                    closedTabPages.Remove(currentNodeIndex);
                }
                tvwMetrics.Nodes[0].Nodes[currentNodeIndex].Remove();
               
                // remove from save deleted panel.
                FileSave.MetricPanes.RemoveAt(currentNodeIndex);

                /*
                 * Must update the save file indices of all panels ahead of the deleted panel.
                 */
                for (int i = currentNodeIndex; i < FileSave.MetricPanes.Count; ++i)
                {
                    if (tabsMetrics.Controls[i].Controls[0] is FrmStatistics)
                    {
                        FrmStatistics form = tabsMetrics.Controls[i].Controls[0] as FrmStatistics;
                        --form.SaveFileIndex;
                    }
                    else if (tabsMetrics.Controls[i].Controls[0] is FrmSMI)
                    {
                        FrmSMI form = tabsMetrics.Controls[i].Controls[0] as FrmSMI;
                        --form.SaveFileIndex;
                    }
                    else
                    {
                        FrmEnterFPData form = tabsMetrics.Controls[i].Controls[0] as FrmEnterFPData;
                        --form.SaveFileIndex;
                    }
                }
                --saveFileIndex; // decrement savefileindex because a panel has been deleted.
            }
        }

        // keeps track of currently closed pages.
        private Dictionary<int, TabPage> closedTabPages = new Dictionary<int, TabPage>();
        /*Part of right click menu for tree view nodes that keeps track of the tabs.*/
        private void mnuClose_Click(object sender, EventArgs e)
        {
            // check if already closed
            if (!closedTabPages.ContainsKey(tvwMetrics.SelectedNode.Index))
            {
                closedTabPages.Add(tvwMetrics.SelectedNode.Index, currentTabPage);
                treeViewToTabs[tvwMetrics.SelectedNode] = null;
                tabsToTreeView.Remove(currentTabPage);
                tabsMetrics.TabPages.Remove(currentTabPage);
            }
        }

        /*Part of right click menu for tree view nodes that keeps track of the tabs.*/
        private void mnuOpen_Click(object sender, EventArgs e)
        {
            // check if already opened
            if (closedTabPages.ContainsKey(tvwMetrics.SelectedNode.Index))
            {
                tabsMetrics.TabPages.Add(closedTabPages[tvwMetrics.SelectedNode.Index]);
                // update tree view index to tab index
                treeViewToTabs[tvwMetrics.SelectedNode] = closedTabPages[tvwMetrics.SelectedNode.Index];
                tabsToTreeView.Add(closedTabPages[tvwMetrics.SelectedNode.Index], tvwMetrics.SelectedNode);
                closedTabPages.Remove(tvwMetrics.SelectedNode.Index);
            }
        }

        private void languagesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form languagePreferenceForm = new FrmLanguagesSelect();
            DialogResult selectedButton =
                languagePreferenceForm.ShowDialog();
            if (selectedButton == DialogResult.OK)
            {
                changedBit = true;
                SelectedLanguage = (Language)languagePreferenceForm.Tag;
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            save();
        }

        private void save()
        {
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.Filter = "Metrics Suite (*.ms)|*.ms";
            saveFile.DefaultExt = "ms";
            saveFile.AddExtension = true;
            saveFile.FileName = FileSave.ProjectName + ".ms";
            if (saveFile.FileName != "")
            {
                System.IO.FileStream fs = (System.IO.FileStream)saveFile.OpenFile();
                System.IO.StreamWriter textOut = new System.IO.StreamWriter(fs);
                fs.SetLength(0);
                textOut.Write(FileSave.ProjectName + "|");
                textOut.Write(FileSave.ProductName + "|");
                textOut.WriteLine(FileSave.Creator);
                textOut.Write(FileSave.Comments);
                textOut.WriteLine("");
                textOut.WriteLine("End-Comments"); // End of Comments
                foreach (SaveMetrics saveMetrics in FileSave.MetricPanes)
                {
                    if (saveMetrics.IsFunctionPoint)
                    {
                        textOut.WriteLine("FP");
                        textOut.Write(saveMetrics.ExternalInputs + "|");
                        textOut.Write(saveMetrics.ExternalOutputs + "|");
                        textOut.Write(saveMetrics.ExternalInquiries + "|");
                        textOut.Write(saveMetrics.InternalLogicalFiles + "|");
                        textOut.Write(saveMetrics.ExternalInterfaceFiles + "|");
                        textOut.Write(saveMetrics.TotalCount + "|");
                        textOut.Write(saveMetrics.ComputeFP + "|");
                        textOut.Write(saveMetrics.ExternalInputsWeight + "|");
                        textOut.Write(saveMetrics.ExternalOutputsWeight + "|");
                        textOut.Write(saveMetrics.ExternalInquiriesWeight + "|");
                        textOut.Write(saveMetrics.InternalLogicalFilesWeight + "|");
                        textOut.Write(saveMetrics.ExternalInterfaceFilesWeight + "|");
                        foreach (decimal index in saveMetrics.CurrentVAFs)
                        {
                            textOut.Write(index + "|");
                        }
                        textOut.Write(saveMetrics.CurrentLanguage.Name + "|");
                        textOut.Write(saveMetrics.FunctionPointTabName + "|");
                        textOut.WriteLine(saveMetrics.CurrentLanguage.Value);
                    }
                    else if (saveMetrics.IsSMI)
                    {
                        // seperate between SMI tab and Function Point Tab
                        textOut.WriteLine("SMI");
                        foreach (SMIColumns row in saveMetrics.RowData)
                        {
                            textOut.Write(row.SMI + "|");
                            textOut.Write(row.ModulesAdded + "|");
                            textOut.Write(row.ModulesChanged + "|");
                            textOut.Write(row.ModulesDeleted + "|");
                            textOut.Write(row.TotalModules + "|");
                            textOut.WriteLine();
                        }
                        textOut.WriteLine("END-SMI");
                    }
                    else
                    {
                        textOut.WriteLine("STATISTICS");
                        textOut.WriteLine(saveMetrics.StatisticsFilePath);
                        textOut.WriteLine("END-STATISTICS");
                    }

                }
                textOut.Close();
            }
            changedBit = false;
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            saveObject = new SaveMetrics();
            SelectedLanguage = new Language();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "Metrics Suite (*.ms)|*.ms";
            openFile.DefaultExt = "ms";
            openFile.ShowDialog();
            if (openFile.FileName != "")
            {
                foreach (TabPage t in tabsMetrics.TabPages)
                {
                    t.Controls[0].Dispose();
                    t.Dispose();
                }
                // clear hashtables of tree index to tab index and vice versa.
                tabsToTreeView.Clear();
                treeViewToTabs.Clear();

                tvwMetrics.Nodes.Clear();     // Clear tree view nodes before opening.
                tabsMetrics.TabPages.Clear(); // Clear the tab pages before opening.
                saveFileIndex = 0;            // Reset save index in FileSave for MetricsPane list.
                saveFile.MetricPanes.Clear(); // Clear save file's list of function point panes
                System.IO.FileStream fs = (System.IO.FileStream)openFile.OpenFile();
                System.IO.StreamReader textIn = new System.IO.StreamReader(fs);
                string row = textIn.ReadLine();
                string[] columns = row.Split('|');
                FileSave.ProjectName = columns[0].ToString();
                FileSave.ProductName = columns[1].ToString();
                FileSave.Creator = columns[2].ToString();
                FileSave.Comments = ""; // clear comments to prevent concatenation of previous comment.
                
                this.Text = "CECS 543 Metrics Suite" + " - " + FileSave.ProjectName; // set form text as project name
                tvwMetrics.Nodes.Add(FileSave.ProductName);                         // add root project name to tree view

                row = textIn.ReadLine();
                while (row != "End-Comments")
                {
                    FileSave.Comments += row.ToString() + "\r\n";
                    row = textIn.ReadLine();
                }
                // trim extra carriage return and new line
                FileSave.Comments = FileSave.Comments.TrimEnd('\r', '\n');

                while (textIn.Peek() != -1)
                {
                    row = textIn.ReadLine();
                    if (row.ToString() == "FP")
                    {
                        saveObject.IsFunctionPoint = true;
                        row = textIn.ReadLine();
                        columns = row.Split('|');
                        saveObject.ExternalInputs = Decimal.Parse(columns[0]);
                        saveObject.ExternalOutputs = Decimal.Parse(columns[1]);
                        saveObject.ExternalInquiries = Decimal.Parse(columns[2]);
                        saveObject.InternalLogicalFiles = Decimal.Parse(columns[3]);
                        saveObject.ExternalInterfaceFiles = Decimal.Parse(columns[4]);
                        saveObject.TotalCount = Decimal.Parse(columns[5]);
                        saveObject.ComputeFP = Decimal.Parse(columns[6]);
                        saveObject.ExternalInputsWeight = Decimal.Parse(columns[7]);
                        saveObject.ExternalOutputsWeight = Decimal.Parse(columns[8]);
                        saveObject.ExternalInquiriesWeight = Decimal.Parse(columns[9]);
                        saveObject.InternalLogicalFilesWeight = Decimal.Parse(columns[10]);
                        saveObject.ExternalInterfaceFilesWeight = Decimal.Parse(columns[11]);
                        for (int i = 0; i < 15; ++i)
                        {
                            saveObject.CurrentVAFs[i] = Int32.Parse(columns[12 + i]);
                        }
                        saveObject.CurrentLanguage.Name = columns[27];
                        saveObject.FunctionPointTabName = columns[28];
                        saveObject.CurrentLanguage.Value = (int)Decimal.Parse(columns[29]);
                        FrmEnterFPData enterFPDataForm = new FrmEnterFPData();
                        enterFPDataForm.Text = saveObject.FunctionPointTabName;
                        enterFPDataForm.mainForm = this;
                        enterFPDataForm.SaveFileIndex = saveFileIndex;
                        enterFPDataForm.SaveObject = (SaveMetrics)saveObject.Clone();
                        createMetricTab(enterFPDataForm);
                    }
                    else if (row.ToString() == "SMI")
                    {
                        saveObject.IsSMI = true;
                        saveObject.RowData.Clear();
                        row = textIn.ReadLine();
                        while (row.ToString() != "END-SMI")
                        {
                            columns = row.Split('|');
                            SMIColumns tempRow = new SMIColumns();
                            tempRow.SMI = columns[0];
                            tempRow.ModulesAdded = columns[1];
                            tempRow.ModulesChanged = columns[2];
                            tempRow.ModulesDeleted = columns[3];
                            tempRow.TotalModules = columns[4];
                            saveObject.RowData.Add(tempRow);
                            row = textIn.ReadLine();
                        }
                        FrmSMI enterSMI = new FrmSMI();
                        enterSMI.mainForm = this;
                        enterSMI.SaveFileIndex = saveFileIndex;
                        enterSMI.SaveObject = (SaveMetrics)saveObject.Clone();
                        createMetricTab(enterSMI);
                    }
                    else
                    {
                        saveObject.IsStatistics = true;
                        saveObject.StatisticsFilePath = "";
                        row = textIn.ReadLine();
                        columns = row.Split('|');
                        saveObject.StatisticsFilePath = columns[0];
                        parseJaveFile(saveObject.StatisticsFilePath);
                        row = textIn.ReadLine();
                    }
                }
                textIn.Close();
            }
            changedBit = false;
            enableMenuStripButtons();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmNewProject newProject = new FrmNewProject();
            newProject.MainForm = this;
            DialogResult dialogResult = newProject.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                foreach (TabPage t in tabsMetrics.TabPages)
                {
                    t.Controls[0].Dispose();
                    t.Dispose();
                }
                tvwMetrics.Nodes.Clear();
                // add project name to tree view
                tvwMetrics.Nodes.Add(saveFile.ProductName);

                enableMenuStripButtons();
            }
            changedBit = true;
        }

        // enable menu buttons after project is created or loaded.
        private void enableMenuStripButtons()
        {
            editToolStripMenuItem.Enabled = true;
            preferencesToolStripMenuItem.Enabled = true;
            metricsToolStripMenuItem.Enabled = true;
            saveToolStripMenuItem.Enabled = true;
            projectCodeToolStripMenuItem.Enabled = true;
        }

        // dictionariess are used to map the treenodes to the tabnodes
        private Dictionary<TreeNode , TabPage> treeViewToTabs = new Dictionary<TreeNode, TabPage>();
        private Dictionary<TabPage, TreeNode> tabsToTreeView = new Dictionary<TabPage, TreeNode>();
        private void createMetricTab(Form enterMetricDataForm)
        {
            // increment index to let the tab know which index to save to in the list.
            ++saveFileIndex;
            // create object
            saveFile.MetricPanes.Add(new SaveMetrics());
            enterMetricDataForm.TopLevel = false;
            enterMetricDataForm.Visible = true;
            enterMetricDataForm.FormBorderStyle = FormBorderStyle.None;
            enterMetricDataForm.Dock = DockStyle.Left;
            // create tab
            currentTabPage = new TabPage(enterMetricDataForm.Text);
            tabsMetrics.TabPages.Add(currentTabPage);
            currentTabPage.Controls.Add(enterMetricDataForm);
            // create tree node in view
            TreeNode newNode = new TreeNode(enterMetricDataForm.Text);
            tvwMetrics.Nodes[0].Nodes.Add(newNode); // add tab to tree.
            treeViewToTabs.Add(newNode, currentTabPage);
            tabsToTreeView.Add(currentTabPage, newNode);
            tvwMetrics.ExpandAll();
        }

        private void enterFPDataToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmFPPaneName enterName = new FrmFPPaneName();
            enterName.StartPosition = FormStartPosition.CenterParent;
            DialogResult dr = enterName.ShowDialog();

            if (dr == DialogResult.OK)
            {   
                // create function point form then create tab for it.
                FrmEnterFPData enterFPDataForm = new FrmEnterFPData();
                enterFPDataForm.Text = (string) enterName.Tag;

                // save file index in list
                enterFPDataForm.SaveFileIndex = saveFileIndex;
                enterFPDataForm.mainForm = this;
                createMetricTab(enterFPDataForm);
                changedBit = true;
            }
        }

        private void enterSMIDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // create SMI form then create tab for it.
            FrmSMI enterSMIDataForm = new FrmSMI();

            // save file index in list.
            enterSMIDataForm.SaveFileIndex = saveFileIndex;
            enterSMIDataForm.mainForm = this;
            createMetricTab(enterSMIDataForm);
            changedBit = true;
        }

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing && changedBit)
            {
                FrmSaveDialog saveDialog = new FrmSaveDialog();
                saveDialog.StartPosition = FormStartPosition.CenterParent;

                DialogResult dialogResult = saveDialog.ShowDialog();
                if (dialogResult == DialogResult.OK)
                {
                    save();
                }
                else if (dialogResult == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
            }

            if (e.CloseReason == CloseReason.WindowsShutDown && changedBit)
            {
                save();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private List<string> StatisticFiles;
        private void addCodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "Java (*.java)|*.java";
            openFile.DefaultExt = "cs";
            openFile.Multiselect = true;
            openFile.Title = "Browse for Java Files";
            DialogResult dr = openFile.ShowDialog();
            if (dr == DialogResult.OK)
            {
                StatisticFiles = new List<string>();
                foreach (string file in openFile.FileNames)
                {
                    StatisticFiles.Add(file);
                }
                if (StatisticFiles.Count > 0)
                {
                    projectCodeToolStripMenuItem1.Enabled = true;
                }
            }
        }

        // event listener for project code button on drop down menu.
        // parses the java file for the metrics.
        private void projectCodeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            foreach (string file in StatisticFiles)
            {
                parseJaveFile(file);  
            }
        }

        private void parseJaveFile(string file)
        {
                // obtain file info for size and name.
                FileInfo info = new FileInfo(file);
                System.IO.FileStream fs = File.OpenRead(file);
                System.IO.StreamReader textIn = new System.IO.StreamReader(fs);
                textIn.Close();

                // initialize object for printing statistics
                JavaMetrics currentFile = new JavaMetrics();

                // intialize parser and lexer then parse java file.
                JavaJavaLexer lexer = new JavaJavaLexer(new ANTLRFileStream(file));
                JavaJavaParser parser = new JavaJavaParser(new CommonTokenStream(lexer));
                parser.compilationUnit();

                // obtain all information for calculating halstead metrics in java files.
                currentFile.FileName = info.Name;
                currentFile.FileLength = info.Length;
                currentFile.WhiteSpace = lexer.ws;
                currentFile.CommentSpace = lexer.commentcount;
                currentFile.TotalOperators = parser.specialcount + parser.keywordCount;
                currentFile.TotalOperands = parser.identcount + lexer.constantcount;
                currentFile.UniqueOperators = JavaMetrics.uniqueSpecial.Count + JavaMetrics.uniqueKeywords.Count;
                currentFile.UniqueOperands = JavaMetrics.uniqueConstants.Count + JavaMetrics.uniqueIdentifiers.Count;

                // create tab for statistics
                FrmStatistics newStatistics = new FrmStatistics();
                newStatistics.StatisticsText = currentFile.PrintStatistics(); // print text to rich text.
                newStatistics.mainForm = this;
                newStatistics.Text = currentFile.FileName;
                newStatistics.SaveFileIndex = saveFileIndex;
                newStatistics.SaveObject = (SaveMetrics)saveObject.Clone();
                createMetricTab(newStatistics);
                FileSave.MetricPanes[newStatistics.SaveFileIndex].StatisticsFilePath = file; // save filepath in the saveobject.
                
                // clear unique storage before checking next file.
                JavaMetrics.uniqueSpecial.Clear();
                JavaMetrics.uniqueKeywords.Clear();
                JavaMetrics.uniqueConstants.Clear();
                JavaMetrics.uniqueIdentifiers.Clear();
                JavaMetrics.mccabeValues.Clear();
        }

        private void tvwMetrics_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (treeViewToTabs[e.Node] != null)
            {
                tabsMetrics.SelectedIndex = tabsMetrics.TabPages.IndexOf(treeViewToTabs[e.Node]);
                currentTabPage = tabsMetrics.SelectedTab;
            }
        }

        private void tabsMetrics_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabsMetrics.TabCount > 0)
            {               
                currentTabPage = tabsMetrics.SelectedTab;
                tvwMetrics.SelectedNode = tabsToTreeView[currentTabPage];
                tvwMetrics.Focus();
            }
        }

        /*
         * Part of right click menu for tree view nodes that keeps track of the tabs.
         */
        private TreeNode oldSelectNode;
        private void tvwMetrics_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Point p = new Point(e.X, e.Y);
                TreeNode node = tvwMetrics.GetNodeAt(p);
                if (node != null)
                {
                    oldSelectNode = tvwMetrics.SelectedNode;
                    tvwMetrics.SelectedNode = node;
                    ctxtMnuTvwMetrics.Show(tvwMetrics, p);
                }

                tvwMetrics.SelectedNode = oldSelectNode;
                oldSelectNode = null;
            }
        }
    }
}
