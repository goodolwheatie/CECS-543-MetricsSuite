﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CECS543MetricsSuite
{
    public class Language : ICloneable
    {
        public string Name { get; set; }
        public int Value { get; set; }
        public Language()
        {
            Name = "None";
            Value = 0;
        }
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
