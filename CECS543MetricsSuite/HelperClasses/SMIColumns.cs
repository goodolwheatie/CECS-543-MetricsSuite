﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CECS543MetricsSuite
{
    public class SMIColumns : ICloneable
    {
        public string SMI { get; set; }
        public string ModulesAdded { get; set; }
        public string ModulesChanged { get; set; }
        public string ModulesDeleted { get; set; }
        public string TotalModules { get; set; }
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
