using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CECS543MetricsSuite
{
    public class JavaMetrics
    {
        public static HashSet<string> uniqueKeywords = new HashSet<string>();
        public static HashSet<string> uniqueSpecial = new HashSet<string>();
        public static HashSet<string> uniqueIdentifiers = new HashSet<string>();
        public static HashSet<string> uniqueConstants = new HashSet<string>();
        public static HashSet<string> mccabeValues = new HashSet<string>();
        public string FileName { get; set; }
        public double FileLength { get; set; }
        public double WhiteSpace { get; set; }
        public double CommentSpace { get; set; }
        public int UniqueOperators { get; set; }
        public int UniqueOperands { get; set; }
        public int TotalOperators { get; set; }
        public int TotalOperands { get; set; }

        private int programLength;
        private int programVocabulary;
        private double volume;
        private double difficulty;
        private double effort;
        private double time;
        private double bugs;

        public void calculateHalstead()
        {
            programVocabulary = UniqueOperators + UniqueOperands;
            programLength = TotalOperators + TotalOperands;
            volume = programLength * Math.Log(programVocabulary, 2.0);
            difficulty = (UniqueOperators / 2.0) * ((double)TotalOperands / UniqueOperands);
            effort = difficulty * volume;
            time = effort / 18;
            bugs = volume / 3000;
        }

        public string PrintStatistics()
        {
            calculateHalstead();
            string header =
                string.Format("File Name: {0}\n" +
                              "File length in bytes: {1}\n" +
                              "File white space: {2}\n" +
                              "File comment space in bytes: {3}\n" +
                              "Comment percentage of file: {4:p3}\n",
                    FileName, FileLength, WhiteSpace, CommentSpace, CommentSpace / FileLength);
            string halstead =
                string.Format("Halstead metrics:\n" +
                              "\tUnique operators: {0}\n" +
                              "\tUnique operands: {1}\n" +
                              "\tTotal operators: {2}\n" +
                              "\tTotal operands: {3}\n" +
                              "\tProgram length (N) = {4}\n" +
                              "\tProgram vocabulary (n) = {5}\n" +
                              "\tVolume = {6:f1}\n" +
                              "\tDifficulty = {7:f1}\n" +
                              "\tEffort = {8:f1} Time = {9:f3} ({10:f3} minutes or {11:f2} hours or {12:f3} person months)\n" +
                              "\tBugs expected = {13:f3}\n\n\n", UniqueOperators, UniqueOperands, TotalOperators, TotalOperands,
                    programLength, programVocabulary, volume, difficulty, effort, time, time / 60, time / 3600,
                    time / 3600 / 730, bugs);
            return header + halstead + printMcCabe();
        }

        public string printMcCabe()
        {
            string mccabe = "McCabe's Cyclomatic Complexity:\n";
            foreach (string s in mccabeValues)
            {
                mccabe += "\t" + s + "\n";
            }
            return mccabe;
        }
    }
}
