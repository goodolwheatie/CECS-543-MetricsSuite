﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CECS543MetricsSuite
{
    public class SaveFile : ICloneable
    {
        public List<SaveMetrics> MetricPanes { get; set; }
        public string ProjectName { get; set; }
        public string ProductName { get; set; }
        public string Creator { get; set; }
        public string Comments { get; set; }

        public SaveFile()
        {
            MetricPanes = new List<SaveMetrics>();
            ProjectName = "";
            ProductName = "";
            Creator = "";
            Comments = "";
        }
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
