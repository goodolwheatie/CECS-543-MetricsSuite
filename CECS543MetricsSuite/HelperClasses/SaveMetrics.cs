﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CECS543MetricsSuite
{
    public class SaveMetrics : ICloneable
    {
        public bool IsFunctionPoint { get; set; }
        public bool IsSMI { get; set; }
        public bool IsStatistics { get; set; }
        public decimal ExternalInputs { get; set; }
        public decimal ExternalOutputs { get; set; }
        public decimal ExternalInquiries { get; set; }
        public decimal InternalLogicalFiles { get; set; }
        public decimal ExternalInterfaceFiles { get; set; }
        public decimal TotalCount { get; set; }
        public decimal ComputeFP { get; set; }
        public decimal ExternalInputsWeight { get; set; }
        public decimal ExternalOutputsWeight { get; set; }
        public decimal ExternalInquiriesWeight { get; set;}
        public decimal InternalLogicalFilesWeight { get; set; }
        public decimal ExternalInterfaceFilesWeight { get; set; }
        public string FunctionPointTabName { get; set; }
        public Language CurrentLanguage { get; set; }
        public List<int> CurrentVAFs { get; set; }
        public List<SMIColumns> RowData { get => rowData; set => rowData = value; }
        private List<SMIColumns> rowData;
        public string StatisticsFilePath { get; set; }

        public SaveMetrics()
        {
            StatisticsFilePath = "";
            CurrentLanguage = new Language();
            RowData = new List<SMIColumns>();
            CurrentVAFs = new List<int>();
            for (int i = 0; i < 15; ++i)
                CurrentVAFs.Add(0);
            ExternalInputs = 0;
            ExternalOutputs = 0;
            ExternalInquiries = 0;
            InternalLogicalFiles = 0;
            ExternalInterfaceFiles = 0;
            ExternalInputsWeight = 1;
            ExternalOutputsWeight = 1;
            ExternalInquiriesWeight = 1;
            InternalLogicalFilesWeight = 1;
            ExternalInterfaceFilesWeight = 1;
            TotalCount = 0;
            ComputeFP = 0;
            FunctionPointTabName = "";
        }
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
