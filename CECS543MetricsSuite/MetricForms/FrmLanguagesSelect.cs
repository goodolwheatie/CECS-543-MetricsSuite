﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CECS543MetricsSuite
{
    public partial class FrmLanguagesSelect : Form
    {
        public FrmLanguagesSelect()
        {
            InitializeComponent();
        }

        private void FrmLanguagesSelect_Load(object sender, EventArgs e)
        {
            // load combo box with languages
            var dataSource = new List<Language>();
            dataSource.Add(new Language() { Name = "Assembler", Value = 119 }); // QSM Source
            dataSource.Add(new Language() { Name = "Ada 95", Value = 49 }); // cs.bsu.edu Source
            dataSource.Add(new Language() { Name = "C", Value = 97 }); // QSM
            dataSource.Add(new Language() { Name = "C++", Value = 50 }); // QSM
            dataSource.Add(new Language() { Name = "C#", Value = 54 }); // QSM
            dataSource.Add(new Language() { Name = "COBOL", Value = 61 }); // QSM
            dataSource.Add(new Language() { Name = "FORTRAN", Value = 107 }); // cs.bsu.edu 
            dataSource.Add(new Language() { Name = "HTML", Value = 34 }); // QSM
            dataSource.Add(new Language() { Name = "Java", Value = 53 }); // QSM
            dataSource.Add(new Language() { Name = "JavaScript", Value = 47 }); // QSM
            dataSource.Add(new Language() { Name = "VBScript", Value = 50 }); // sunset.usc.edu Source
            dataSource.Add(new Language() { Name = "Visual Basic", Value = 42 }); // QSM

            this.cboLanguages.DataSource = dataSource;
            this.cboLanguages.DisplayMember = "Name";
            this.cboLanguages.ValueMember = "Value";

            this.cboLanguages.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void btnDoneLanguages_Click(object sender, EventArgs e)
        {
            this.Tag = cboLanguages.SelectedItem;
            this.Close();
        }
    }
}
