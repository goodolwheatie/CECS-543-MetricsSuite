﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CECS543MetricsSuite
{
    public partial class FrmEnterFPData : Form
    {
        private const decimal FunctionPointConstant_065 = (decimal)0.65;
        private const decimal FunctionPointConstant_001 = (decimal)0.01;
        public int SaveFileIndex { get; set; } // store index of save in list from main form.
        private Language selectedLanguage; // store the selected language for referencing.
        private decimal codeSize = 0;
        private decimal lineOfCodePerFunctionPoint = 0;
        private int currentVAF = 0;
        private decimal functionPointTotal = 0;
        private decimal totalCount = 0;
        public FrmMain mainForm; // reference to main form
        public SaveMetrics SaveObject { set; get; } // store a save object for function points

        public FrmEnterFPData()
        {
            InitializeComponent();
        }

        private void FrmEnterFPData_Load(object sender, EventArgs e)
        {
            mainForm.FileSave.MetricPanes[SaveFileIndex].IsFunctionPoint = true;
            mainForm.FileSave.MetricPanes[SaveFileIndex].FunctionPointTabName = Text;
            // check if loading from a file
            if (SaveObject == null)
            {
                selectedLanguage = mainForm.SelectedLanguage;
                if (selectedLanguage != null)
                {
                    txtLanguage.Text = selectedLanguage.Name;
                    lineOfCodePerFunctionPoint = selectedLanguage.Value;
                    mainForm.FileSave.MetricPanes[SaveFileIndex].CurrentLanguage = selectedLanguage;
                }
            }
            else
            {
                loadDataFromSave();
            }
        }

        private void btnChangeLanguage_Click(object sender, EventArgs e)
        {
            Form languagePreferenceForm = new FrmLanguagesSelect();
            DialogResult selectedButton =
                languagePreferenceForm.ShowDialog();
            if (selectedButton == DialogResult.OK)
            {
                mainForm.changedBit = true; // file changed
                selectedLanguage = (Language)languagePreferenceForm.Tag;
                txtLanguage.Text = selectedLanguage.Name;
                lineOfCodePerFunctionPoint = selectedLanguage.Value;
                mainForm.FileSave.MetricPanes[SaveFileIndex].CurrentLanguage = selectedLanguage; // save
            }
        }

        // determines which radio box is chosen then calculates the count with weights for respective boxes.
        private int calculateWithWeights
            (NumericUpDown upd, Panel pnl, TextBox tb)
        {
            var rb = pnl.Controls.OfType<RadioButton>()
                .FirstOrDefault(r => r.Checked);
            tb.Text = ((upd.Value) * Decimal.Parse(rb.Text)).ToString();
            if (tb.Text == "0")
                tb.Text = String.Empty;
            return pnl.Controls.IndexOf(rb);
        }

        private void updInputs_ValueChanged(object sender, EventArgs e)
        {
            mainForm.changedBit = true; // file changed
            mainForm.FileSave.MetricPanes[SaveFileIndex].ExternalInputsWeight = 
            calculateWithWeights(updInputs, pnlExternalInputsRadio,
                txtExternalInputsWithWeights);
            mainForm.FileSave.MetricPanes[SaveFileIndex].ExternalInputs = updInputs.Value; // save
        }

        private void updOutputs_ValueChanged(object sender, EventArgs e)
        {
            mainForm.changedBit = true; // file changed
            mainForm.FileSave.MetricPanes[SaveFileIndex].ExternalOutputsWeight =
            calculateWithWeights(updOutputs, pnlExternalOutputsRadio,
                txtExternalOutputsWithWeights);
            mainForm.FileSave.MetricPanes[SaveFileIndex].ExternalOutputs = updOutputs.Value; // save
        }

        private void updInquiries_ValueChanged(object sender, EventArgs e)
        {
            mainForm.changedBit = true; // file changed
            mainForm.FileSave.MetricPanes[SaveFileIndex].ExternalInquiriesWeight =
            calculateWithWeights(updInquiries, pnlExternalInquiriesRadio, 
                txtInquiriesWithWeights);
            mainForm.FileSave.MetricPanes[SaveFileIndex].ExternalInquiries = updInquiries.Value; // save
        }
        
        private void updLogical_ValueChanged(object sender, EventArgs e)
        {
            mainForm.changedBit = true; // file changed
            mainForm.FileSave.MetricPanes[SaveFileIndex].InternalLogicalFilesWeight = 
            calculateWithWeights(updLogical, pnlInternalLogicRadio,
                txtLogicalWithWeights);
            mainForm.FileSave.MetricPanes[SaveFileIndex].InternalLogicalFiles = updLogical.Value; // save
        }

        private void updInterface_ValueChanged(object sender, EventArgs e)
        {
            mainForm.changedBit = true; // file changed
            mainForm.FileSave.MetricPanes[SaveFileIndex].ExternalInterfaceFilesWeight =
            calculateWithWeights(updInterface, pnlExternalInterfaceRadio,
                txtInterfaceWithWeights);
            mainForm.FileSave.MetricPanes[SaveFileIndex].ExternalInterfaceFiles = updInterface.Value; // save
        }

        private void calculateTotalCount(object sender, EventArgs e)
        {
            // obtain user data from text boxes
            decimal inputsCount, outputsCount, inquiriesCount,
                logicalCount, interfaceCount;
            if (!Decimal.TryParse(txtExternalInputsWithWeights.Text, out inputsCount))
                inputsCount = 0;
            if (!Decimal.TryParse(txtExternalOutputsWithWeights.Text, out outputsCount))
                outputsCount = 0;
            if (!Decimal.TryParse(txtInquiriesWithWeights.Text, out inquiriesCount))
                inquiriesCount = 0;
            if (!Decimal.TryParse(txtLogicalWithWeights.Text, out logicalCount))
                logicalCount = 0;
            if (!Decimal.TryParse(txtInterfaceWithWeights.Text, out interfaceCount))
                interfaceCount = 0;
            totalCount = (inputsCount + outputsCount + inquiriesCount + logicalCount + interfaceCount);
            txtTotalCount.Text = String.Format("{0}", totalCount);
            mainForm.FileSave.MetricPanes[SaveFileIndex].TotalCount = totalCount;
            if (txtTotalCount.Text == "0")
                txtTotalCount.Text = String.Empty;
        }

        // store VAF values from the VAF dialog
        List<int> indicesVAF;
        private void btnValueAdjustments_Click(object sender, EventArgs e)
        {
            FrmVAF newVAF = new FrmVAF();
            if (indicesVAF != null)
            {
                // if VAF list already exists
                newVAF.CboSelectedIndices = indicesVAF;
            }
            DialogResult dialogResult =
                newVAF.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                mainForm.changedBit = true; // file changed
                // retrieve new VAF list from dialog
                indicesVAF = (List<int>)newVAF.Tag;
                // grab total VAF from end of list.
                txtValueAdjustments.Text = indicesVAF[14].ToString();
                currentVAF = indicesVAF[14];
                mainForm.FileSave.MetricPanes[SaveFileIndex].CurrentVAFs = indicesVAF;
            }
        }

        private void btnComputeFP_Click(object sender, EventArgs e)
        {
            mainForm.changedBit = true; // file changed
            // calculate function point total
            functionPointTotal = totalCount * 
                (FunctionPointConstant_065 + FunctionPointConstant_001 * currentVAF);
            mainForm.FileSave.MetricPanes[SaveFileIndex].ComputeFP = functionPointTotal;
            txtComputeFP.Text = String.Format("{0:n}", functionPointTotal);
        }

        private void btnComputerCodeSize_Click(object sender, EventArgs e)
        {
            // calculate code size
            codeSize = functionPointTotal * lineOfCodePerFunctionPoint;
            txtComputeCodeSize.Text = String.Format("{0:n0}", codeSize);
        }

        private void loadDataFromSave()
        {
            indicesVAF = SaveObject.CurrentVAFs;
            txtValueAdjustments.Text = SaveObject.CurrentVAFs[14].ToString();
            updInputs.Value = SaveObject.ExternalInputs;
            updOutputs.Value = SaveObject.ExternalOutputs;
            updInquiries.Value = SaveObject.ExternalInquiries;
            updLogical.Value = SaveObject.InternalLogicalFiles;
            updInterface.Value = SaveObject.ExternalInterfaceFiles;

            totalCount = SaveObject.TotalCount; // check if equal to zero so textbox will be empty.
            if (totalCount != 0)
            {
                txtTotalCount.Text = totalCount.ToString();
            }
            functionPointTotal = SaveObject.ComputeFP; // check if equal to zero so textbox will be empty.

            if (functionPointTotal != 0)
            {
                txtComputeFP.Text = functionPointTotal.ToString("n");
            }

            setRadioButtonCheckedFromSave((RadioButton)
                pnlExternalInputsRadio.Controls[(int)SaveObject.ExternalInputsWeight]);
            setRadioButtonCheckedFromSave((RadioButton)
                pnlExternalOutputsRadio.Controls[(int)SaveObject.ExternalOutputsWeight]);
            setRadioButtonCheckedFromSave((RadioButton)
                pnlExternalInquiriesRadio.Controls[(int)SaveObject.ExternalInquiriesWeight]);
            setRadioButtonCheckedFromSave((RadioButton)
                pnlInternalLogicRadio.Controls[(int)SaveObject.InternalLogicalFilesWeight]);
            setRadioButtonCheckedFromSave((RadioButton)
                pnlExternalInterfaceRadio.Controls[(int)SaveObject.ExternalInterfaceFilesWeight]);
            selectedLanguage = (Language) SaveObject.CurrentLanguage.Clone();
            txtLanguage.Text = selectedLanguage.Name;
            if (selectedLanguage.Name == "")
                txtLanguage.Text = "None";
            lineOfCodePerFunctionPoint = selectedLanguage.Value;
        }
        
        private void setRadioButtonCheckedFromSave(RadioButton radBtn)
        {
            radBtn.Checked = true;
        }
    }
}
