﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CECS543MetricsSuite
{
    public partial class FrmStatistics : Form
    {
        public int SaveFileIndex { get; set; } // store index of save in list from main form.
        public SaveMetrics SaveObject { set; get; } // store a save object for function points
        public FrmMain mainForm; // reference to main form
        public string StatisticsText { get; set; }
        public FrmStatistics()
        {
            InitializeComponent();
        }

        private void FrmStatistics_Load(object sender, EventArgs e)
        {
            rtxtStatistics.Text = StatisticsText;
            mainForm.FileSave.MetricPanes[SaveFileIndex].IsStatistics = true;
        }
    }
}
