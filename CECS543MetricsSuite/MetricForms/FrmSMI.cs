﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CECS543MetricsSuite
{
    public partial class FrmSMI : Form
    {
        private decimal SMI = 0;
        private decimal modulesAdded = 0;
        private decimal modulesChanged = 0;
        private decimal modulesDeleted = 0;
        private decimal totalModules = 0;
        private bool isCurrentRowIndexCalculated;

        public int SaveFileIndex { get; set; } // store index of save in list from main form
        string columnHeader; // store name of column
        DataGridViewCell cell; // store current cell last edited
        int currentRowIndex = 0; // store row index
        public FrmMain mainForm; // reference to main form
        SMIColumns currentRow = new SMIColumns(); // store temporary object for current row.
        public SaveMetrics SaveObject { set; get; } // store a save object for SMI

        public FrmSMI()
        {
            InitializeComponent();
        }

        private void FrmSMI_Load(object sender, EventArgs e)
        {
            if (SaveObject != null)
            {
                currentRowIndex = 0;
                loadFromSave();
            }
            mainForm.FileSave.MetricPanes[SaveFileIndex].IsSMI = true;
            clmSMI.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            clmModulesAdded.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            clmModulesChanged.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            clmModulesDeleted.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            clmTotalModules.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private void loadFromSave()
        {
            isCurrentRowIndexCalculated = true;
            if (SaveObject != null)
            {
                foreach (SMIColumns row in SaveObject.RowData)
                {
                    dgvSMI.Rows.Add();
                    dgvSMI.Rows[currentRowIndex].Cells[0].Value = row.SMI;
                    dgvSMI.Rows[currentRowIndex].Cells[1].Value = row.ModulesAdded;
                    dgvSMI.Rows[currentRowIndex].Cells[2].Value = row.ModulesChanged;
                    dgvSMI.Rows[currentRowIndex].Cells[3].Value = row.ModulesDeleted;
                    dgvSMI.Rows[currentRowIndex].Cells[4].Value = row.TotalModules;
                    ++currentRowIndex;
                }

                if (SaveObject.RowData.Count > 0)
                {
                    currentRowIndex -= 1;
                    SMI = Decimal.Parse(SaveObject.RowData[currentRowIndex].SMI);
                    modulesAdded = Decimal.Parse(SaveObject.RowData[currentRowIndex].ModulesAdded);
                    modulesChanged = Decimal.Parse(SaveObject.RowData[currentRowIndex].ModulesChanged);
                    modulesDeleted = Decimal.Parse(SaveObject.RowData[currentRowIndex].ModulesDeleted);
                    totalModules = Decimal.Parse(SaveObject.RowData[currentRowIndex].TotalModules);
                    mainForm.FileSave.MetricPanes[SaveFileIndex].RowData = 
                        new List<SMIColumns>(SaveObject.RowData);
                }
            }
        }

        private void btnAddRow_Click(object sender, EventArgs e)
        {
            if (isCurrentRowIndexCalculated || dgvSMI.Rows.Count == 0)
            {
                modulesAdded = 0;
                modulesChanged = 0;
                modulesDeleted = 0;
                isCurrentRowIndexCalculated = false;
                dgvSMI.Rows.Add();
                int rowCount = dgvSMI.RowCount;
                if (rowCount == 1)
                {
                    dgvSMI.Rows[0].Cells[2].Value = 0;
                    dgvSMI.Rows[0].Cells[3].Value = 0;
                    dgvSMI.Rows[0].Cells[2].ReadOnly = true; // set first row "Modules Changed" to read only.
                    dgvSMI.Rows[0].Cells[3].ReadOnly = true; // set first row "Modules Deleted" to read only.
                }
                else if (rowCount > 1)
                {
                    dgvSMI.Rows[rowCount - 2].Cells[1].ReadOnly = true;
                    dgvSMI.Rows[rowCount - 2].Cells[2].ReadOnly = true;
                    dgvSMI.Rows[rowCount - 2].Cells[3].ReadOnly = true;
                }
            }
            else
            {
                MessageBox.Show("You must calculate current row's SMI before adding another row.", "Error", 
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }

        private void dgvSMI_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            mainForm.changedBit = true; // save file
        }

        private void dgvSMI_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            columnHeader = dgvSMI.Columns[e.ColumnIndex].HeaderText;
            cell = dgvSMI.Rows[e.RowIndex].Cells[e.ColumnIndex];
            currentRowIndex = e.RowIndex;
            switch (columnHeader)
            {
                case "Modules Added":
                    if (!Decimal.TryParse((string)cell.Value, out modulesAdded))
                    {
                        cell.Value = "";
                    }
                    break;
                case "Modules Changed":
                    if (!Decimal.TryParse((string)cell.Value, out modulesChanged))
                    {
                        cell.Value = "";
                    }
                    break;
                case "Modules Deleted":
                    if (!Decimal.TryParse((string)cell.Value, out modulesDeleted))
                    {
                        cell.Value = "";

                    }
                    break;
            }
        }

        private void btnComputeIndex_Click(object sender, EventArgs e)
        {
            if (!isCurrentRowIndexCalculated)
            {
                if (dgvSMI.RowCount > 0)
                {
                    currentRowIndex = dgvSMI.Rows.Count - 1;
                    isCurrentRowIndexCalculated = true;
                    currentRow = new SMIColumns();
                    // calculate totalModules
                    totalModules += modulesAdded - modulesDeleted;
                    dgvSMI.Rows[currentRowIndex].Cells[4].Value = totalModules;

                    // calculate SMI
                    SMI = (totalModules -
                           (modulesAdded + modulesChanged + modulesDeleted)) / totalModules;
                    dgvSMI.Rows[currentRowIndex].Cells[0].Value = SMI;

                    if (dgvSMI.RowCount >= 1)
                    {
                        currentRow.ModulesAdded = modulesAdded.ToString();
                        currentRow.ModulesChanged = modulesChanged.ToString();
                        currentRow.ModulesDeleted = modulesDeleted.ToString();
                        currentRow.SMI = SMI.ToString();
                        currentRow.TotalModules = totalModules.ToString();
                        mainForm.FileSave.MetricPanes[SaveFileIndex].RowData.Add(currentRow);
                    }
                }  
            }
            else
            {
                MessageBox.Show("You already calulated the SMI for this row.", "Error", 
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
