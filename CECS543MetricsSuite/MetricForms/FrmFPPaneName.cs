﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CECS543MetricsSuite
{
    public partial class FrmFPPaneName : Form
    {
        public FrmFPPaneName()
        {
            InitializeComponent();
        }

        private void btnOkay_Click(object sender, EventArgs e)
        {
            Tag = txtName.Text;
        }
    }
}
