﻿namespace CECS543MetricsSuite
{
    partial class FrmSMI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvSMI = new System.Windows.Forms.DataGridView();
            this.clmSMI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmModulesAdded = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmModulesChanged = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmModulesDeleted = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmTotalModules = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAddRow = new System.Windows.Forms.Button();
            this.btnComputeIndex = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSMI)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvSMI
            // 
            this.dgvSMI.AllowUserToAddRows = false;
            this.dgvSMI.AllowUserToDeleteRows = false;
            this.dgvSMI.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSMI.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmSMI,
            this.clmModulesAdded,
            this.clmModulesChanged,
            this.clmModulesDeleted,
            this.clmTotalModules});
            this.dgvSMI.Location = new System.Drawing.Point(9, 85);
            this.dgvSMI.Margin = new System.Windows.Forms.Padding(2);
            this.dgvSMI.Name = "dgvSMI";
            this.dgvSMI.RowTemplate.Height = 24;
            this.dgvSMI.Size = new System.Drawing.Size(640, 298);
            this.dgvSMI.TabIndex = 0;
            this.dgvSMI.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSMI_CellEndEdit);
            this.dgvSMI.UserAddedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvSMI_UserAddedRow);
            // 
            // clmSMI
            // 
            this.clmSMI.HeaderText = "SMI";
            this.clmSMI.Name = "clmSMI";
            this.clmSMI.ReadOnly = true;
            // 
            // clmModulesAdded
            // 
            this.clmModulesAdded.HeaderText = "Modules Added";
            this.clmModulesAdded.Name = "clmModulesAdded";
            // 
            // clmModulesChanged
            // 
            this.clmModulesChanged.HeaderText = "Modules Changed";
            this.clmModulesChanged.Name = "clmModulesChanged";
            // 
            // clmModulesDeleted
            // 
            this.clmModulesDeleted.HeaderText = "Modules Deleted";
            this.clmModulesDeleted.Name = "clmModulesDeleted";
            // 
            // clmTotalModules
            // 
            this.clmTotalModules.HeaderText = "Total Modules";
            this.clmTotalModules.Name = "clmTotalModules";
            this.clmTotalModules.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 50);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Software Maturity Index";
            // 
            // btnAddRow
            // 
            this.btnAddRow.AutoSize = true;
            this.btnAddRow.Location = new System.Drawing.Point(9, 414);
            this.btnAddRow.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddRow.Name = "btnAddRow";
            this.btnAddRow.Size = new System.Drawing.Size(86, 23);
            this.btnAddRow.TabIndex = 2;
            this.btnAddRow.Text = "Add Row";
            this.btnAddRow.UseVisualStyleBackColor = true;
            this.btnAddRow.Click += new System.EventHandler(this.btnAddRow_Click);
            // 
            // btnComputeIndex
            // 
            this.btnComputeIndex.AutoSize = true;
            this.btnComputeIndex.Location = new System.Drawing.Point(119, 414);
            this.btnComputeIndex.Margin = new System.Windows.Forms.Padding(2);
            this.btnComputeIndex.Name = "btnComputeIndex";
            this.btnComputeIndex.Size = new System.Drawing.Size(88, 23);
            this.btnComputeIndex.TabIndex = 3;
            this.btnComputeIndex.Text = "Compute Index";
            this.btnComputeIndex.UseVisualStyleBackColor = true;
            this.btnComputeIndex.Click += new System.EventHandler(this.btnComputeIndex_Click);
            // 
            // FrmSMI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(858, 443);
            this.ControlBox = false;
            this.Controls.Add(this.btnComputeIndex);
            this.Controls.Add(this.btnAddRow);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvSMI);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSMI";
            this.Text = "SMI";
            this.Load += new System.EventHandler(this.FrmSMI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSMI)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvSMI;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddRow;
        private System.Windows.Forms.Button btnComputeIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmSMI;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmModulesAdded;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmModulesChanged;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmModulesDeleted;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmTotalModules;
    }
}