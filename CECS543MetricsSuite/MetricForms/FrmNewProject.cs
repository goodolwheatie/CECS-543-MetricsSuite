﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CECS543MetricsSuite
{
    public partial class FrmNewProject : Form
    {
        public FrmMain MainForm { get; set; }
        private bool okayButtonClicked = false;
        public FrmNewProject()
        {
            InitializeComponent();
        }

        private void btnOkay_Click(object sender, EventArgs e)
        {
            okayButtonClicked = true;
        }

        private void FrmNewProject_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (okayButtonClicked)
            {
                if (String.IsNullOrEmpty(txtProjectName.Text))
                {
                    MessageBox.Show("Project name cannont be empty.", "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }
                else if (String.IsNullOrEmpty(txtProductName.Text))
                {
                    MessageBox.Show("Product name cannont be empty.", "Error",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }
                else if (String.IsNullOrEmpty(txtCreator.Text))
                {
                    MessageBox.Show("Creator name cannont be empty.", "Error",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }
                else
                {
                    MainForm.FileSave.ProjectName = txtProjectName.Text.Trim();
                    MainForm.FileSave.ProductName = txtProductName.Text.Trim();
                    MainForm.FileSave.Creator = txtCreator.Text.Trim();
                    MainForm.FileSave.Comments = rchComments.Text.Trim();
                    MainForm.Text = "CECS 543 Metrics Suite" + " - " + MainForm.FileSave.ProjectName;
                }
                okayButtonClicked = false;
            }
        }
    }
}

