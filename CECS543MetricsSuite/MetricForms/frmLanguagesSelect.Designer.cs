﻿namespace CECS543MetricsSuite
{
    partial class FrmLanguagesSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblLanguagesSelect = new System.Windows.Forms.Label();
            this.cboLanguages = new System.Windows.Forms.ComboBox();
            this.btnDoneLanguages = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblLanguagesSelect
            // 
            this.lblLanguagesSelect.AutoSize = true;
            this.lblLanguagesSelect.Location = new System.Drawing.Point(13, 13);
            this.lblLanguagesSelect.Name = "lblLanguagesSelect";
            this.lblLanguagesSelect.Size = new System.Drawing.Size(105, 13);
            this.lblLanguagesSelect.TabIndex = 12;
            this.lblLanguagesSelect.Text = "Select one language";
            // 
            // cboLanguages
            // 
            this.cboLanguages.FormattingEnabled = true;
            this.cboLanguages.Location = new System.Drawing.Point(16, 41);
            this.cboLanguages.Name = "cboLanguages";
            this.cboLanguages.Size = new System.Drawing.Size(162, 21);
            this.cboLanguages.TabIndex = 13;
            // 
            // btnDoneLanguages
            // 
            this.btnDoneLanguages.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnDoneLanguages.Location = new System.Drawing.Point(16, 77);
            this.btnDoneLanguages.Name = "btnDoneLanguages";
            this.btnDoneLanguages.Size = new System.Drawing.Size(75, 23);
            this.btnDoneLanguages.TabIndex = 14;
            this.btnDoneLanguages.Text = "Done";
            this.btnDoneLanguages.UseVisualStyleBackColor = true;
            this.btnDoneLanguages.Click += new System.EventHandler(this.btnDoneLanguages_Click);
            // 
            // frmLanguagesSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(207, 124);
            this.Controls.Add(this.btnDoneLanguages);
            this.Controls.Add(this.cboLanguages);
            this.Controls.Add(this.lblLanguagesSelect);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frmLanguagesSelect";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.FrmLanguagesSelect_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblLanguagesSelect;
        private System.Windows.Forms.ComboBox cboLanguages;
        private System.Windows.Forms.Button btnDoneLanguages;
    }
}