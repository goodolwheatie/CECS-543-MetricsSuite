﻿namespace CECS543MetricsSuite
{
    partial class FrmEnterFPData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFunctionPointsTitle = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pnlExternalInputsRadio = new System.Windows.Forms.Panel();
            this.radComplexInputs = new System.Windows.Forms.RadioButton();
            this.radAvgInputs = new System.Windows.Forms.RadioButton();
            this.radSimpleInputs = new System.Windows.Forms.RadioButton();
            this.pnlExternalOutputsRadio = new System.Windows.Forms.Panel();
            this.radOutputsComplex = new System.Windows.Forms.RadioButton();
            this.radOutputsAverage = new System.Windows.Forms.RadioButton();
            this.radOutputsSimple = new System.Windows.Forms.RadioButton();
            this.pnlExternalInquiriesRadio = new System.Windows.Forms.Panel();
            this.radInquiriesComplex = new System.Windows.Forms.RadioButton();
            this.radInquiriesAverage = new System.Windows.Forms.RadioButton();
            this.radInquiriesSimple = new System.Windows.Forms.RadioButton();
            this.pnlInternalLogicRadio = new System.Windows.Forms.Panel();
            this.radLogicalComplex = new System.Windows.Forms.RadioButton();
            this.radLogicalAverage = new System.Windows.Forms.RadioButton();
            this.radLogicalSimple = new System.Windows.Forms.RadioButton();
            this.pnlExternalInterfaceRadio = new System.Windows.Forms.Panel();
            this.radInterfaceComplex = new System.Windows.Forms.RadioButton();
            this.radInterfaceAverage = new System.Windows.Forms.RadioButton();
            this.radInterfaceSimple = new System.Windows.Forms.RadioButton();
            this.txtInterfaceWithWeights = new System.Windows.Forms.TextBox();
            this.txtLogicalWithWeights = new System.Windows.Forms.TextBox();
            this.txtInquiriesWithWeights = new System.Windows.Forms.TextBox();
            this.txtExternalOutputsWithWeights = new System.Windows.Forms.TextBox();
            this.txtExternalInputsWithWeights = new System.Windows.Forms.TextBox();
            this.txtTotalCount = new System.Windows.Forms.TextBox();
            this.btnComputeFP = new System.Windows.Forms.Button();
            this.btnValueAdjustments = new System.Windows.Forms.Button();
            this.btnComputerCodeSize = new System.Windows.Forms.Button();
            this.btnChangeLanguage = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.txtComputeFP = new System.Windows.Forms.TextBox();
            this.txtValueAdjustments = new System.Windows.Forms.TextBox();
            this.txtComputeCodeSize = new System.Windows.Forms.TextBox();
            this.txtLanguage = new System.Windows.Forms.TextBox();
            this.updInputs = new System.Windows.Forms.NumericUpDown();
            this.updOutputs = new System.Windows.Forms.NumericUpDown();
            this.updInquiries = new System.Windows.Forms.NumericUpDown();
            this.updLogical = new System.Windows.Forms.NumericUpDown();
            this.updInterface = new System.Windows.Forms.NumericUpDown();
            this.pnlExternalInputsRadio.SuspendLayout();
            this.pnlExternalOutputsRadio.SuspendLayout();
            this.pnlExternalInquiriesRadio.SuspendLayout();
            this.pnlInternalLogicRadio.SuspendLayout();
            this.pnlExternalInterfaceRadio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.updInputs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updOutputs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updInquiries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updLogical)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updInterface)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFunctionPointsTitle
            // 
            this.lblFunctionPointsTitle.AutoSize = true;
            this.lblFunctionPointsTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFunctionPointsTitle.Location = new System.Drawing.Point(381, 45);
            this.lblFunctionPointsTitle.Name = "lblFunctionPointsTitle";
            this.lblFunctionPointsTitle.Size = new System.Drawing.Size(250, 33);
            this.lblFunctionPointsTitle.TabIndex = 0;
            this.lblFunctionPointsTitle.Text = "Weighting Factors";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 133);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 15);
            this.label1.TabIndex = 6;
            this.label1.Text = "External Inputs";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 166);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 15);
            this.label2.TabIndex = 7;
            this.label2.Text = "External Outputs";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 199);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 15);
            this.label3.TabIndex = 8;
            this.label3.Text = "External Inquiries";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 232);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 15);
            this.label4.TabIndex = 9;
            this.label4.Text = "Internal Logical Files";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 265);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(131, 15);
            this.label5.TabIndex = 10;
            this.label5.Text = "External Interface Files";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(314, 95);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 20);
            this.label6.TabIndex = 26;
            this.label6.Text = "Simple";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(444, 95);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 20);
            this.label7.TabIndex = 27;
            this.label7.Text = "Average";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(585, 95);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 20);
            this.label8.TabIndex = 28;
            this.label8.Text = "Complex";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 319);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(85, 18);
            this.label9.TabIndex = 29;
            this.label9.Text = "Total Count";
            // 
            // pnlExternalInputsRadio
            // 
            this.pnlExternalInputsRadio.Controls.Add(this.radComplexInputs);
            this.pnlExternalInputsRadio.Controls.Add(this.radAvgInputs);
            this.pnlExternalInputsRadio.Controls.Add(this.radSimpleInputs);
            this.pnlExternalInputsRadio.Location = new System.Drawing.Point(268, 130);
            this.pnlExternalInputsRadio.Name = "pnlExternalInputsRadio";
            this.pnlExternalInputsRadio.Size = new System.Drawing.Size(473, 20);
            this.pnlExternalInputsRadio.TabIndex = 35;
            // 
            // radComplexInputs
            // 
            this.radComplexInputs.AutoSize = true;
            this.radComplexInputs.Location = new System.Drawing.Point(332, 0);
            this.radComplexInputs.Name = "radComplexInputs";
            this.radComplexInputs.Size = new System.Drawing.Size(31, 17);
            this.radComplexInputs.TabIndex = 36;
            this.radComplexInputs.Text = "6";
            this.radComplexInputs.UseVisualStyleBackColor = true;
            this.radComplexInputs.CheckedChanged += new System.EventHandler(this.updInputs_ValueChanged);
            // 
            // radAvgInputs
            // 
            this.radAvgInputs.AutoSize = true;
            this.radAvgInputs.Checked = true;
            this.radAvgInputs.Location = new System.Drawing.Point(191, 0);
            this.radAvgInputs.Name = "radAvgInputs";
            this.radAvgInputs.Size = new System.Drawing.Size(31, 17);
            this.radAvgInputs.TabIndex = 36;
            this.radAvgInputs.TabStop = true;
            this.radAvgInputs.Text = "4";
            this.radAvgInputs.UseVisualStyleBackColor = true;
            this.radAvgInputs.CheckedChanged += new System.EventHandler(this.updInputs_ValueChanged);
            // 
            // radSimpleInputs
            // 
            this.radSimpleInputs.AutoSize = true;
            this.radSimpleInputs.Location = new System.Drawing.Point(50, 0);
            this.radSimpleInputs.Name = "radSimpleInputs";
            this.radSimpleInputs.Size = new System.Drawing.Size(31, 17);
            this.radSimpleInputs.TabIndex = 0;
            this.radSimpleInputs.Text = "3";
            this.radSimpleInputs.UseVisualStyleBackColor = true;
            this.radSimpleInputs.CheckedChanged += new System.EventHandler(this.updInputs_ValueChanged);
            // 
            // pnlExternalOutputsRadio
            // 
            this.pnlExternalOutputsRadio.Controls.Add(this.radOutputsComplex);
            this.pnlExternalOutputsRadio.Controls.Add(this.radOutputsAverage);
            this.pnlExternalOutputsRadio.Controls.Add(this.radOutputsSimple);
            this.pnlExternalOutputsRadio.Location = new System.Drawing.Point(268, 165);
            this.pnlExternalOutputsRadio.Name = "pnlExternalOutputsRadio";
            this.pnlExternalOutputsRadio.Size = new System.Drawing.Size(473, 20);
            this.pnlExternalOutputsRadio.TabIndex = 36;
            // 
            // radOutputsComplex
            // 
            this.radOutputsComplex.AutoSize = true;
            this.radOutputsComplex.Location = new System.Drawing.Point(332, 0);
            this.radOutputsComplex.Name = "radOutputsComplex";
            this.radOutputsComplex.Size = new System.Drawing.Size(31, 17);
            this.radOutputsComplex.TabIndex = 36;
            this.radOutputsComplex.Text = "7";
            this.radOutputsComplex.UseVisualStyleBackColor = true;
            this.radOutputsComplex.CheckedChanged += new System.EventHandler(this.updOutputs_ValueChanged);
            // 
            // radOutputsAverage
            // 
            this.radOutputsAverage.AutoSize = true;
            this.radOutputsAverage.Checked = true;
            this.radOutputsAverage.Location = new System.Drawing.Point(191, 0);
            this.radOutputsAverage.Name = "radOutputsAverage";
            this.radOutputsAverage.Size = new System.Drawing.Size(31, 17);
            this.radOutputsAverage.TabIndex = 36;
            this.radOutputsAverage.TabStop = true;
            this.radOutputsAverage.Text = "5";
            this.radOutputsAverage.UseVisualStyleBackColor = true;
            this.radOutputsAverage.CheckedChanged += new System.EventHandler(this.updOutputs_ValueChanged);
            // 
            // radOutputsSimple
            // 
            this.radOutputsSimple.AutoSize = true;
            this.radOutputsSimple.Location = new System.Drawing.Point(50, 0);
            this.radOutputsSimple.Name = "radOutputsSimple";
            this.radOutputsSimple.Size = new System.Drawing.Size(31, 17);
            this.radOutputsSimple.TabIndex = 0;
            this.radOutputsSimple.Text = "4";
            this.radOutputsSimple.UseVisualStyleBackColor = true;
            this.radOutputsSimple.CheckedChanged += new System.EventHandler(this.updOutputs_ValueChanged);
            // 
            // pnlExternalInquiriesRadio
            // 
            this.pnlExternalInquiriesRadio.Controls.Add(this.radInquiriesComplex);
            this.pnlExternalInquiriesRadio.Controls.Add(this.radInquiriesAverage);
            this.pnlExternalInquiriesRadio.Controls.Add(this.radInquiriesSimple);
            this.pnlExternalInquiriesRadio.Location = new System.Drawing.Point(268, 199);
            this.pnlExternalInquiriesRadio.Name = "pnlExternalInquiriesRadio";
            this.pnlExternalInquiriesRadio.Size = new System.Drawing.Size(473, 20);
            this.pnlExternalInquiriesRadio.TabIndex = 37;
            // 
            // radInquiriesComplex
            // 
            this.radInquiriesComplex.AutoSize = true;
            this.radInquiriesComplex.Location = new System.Drawing.Point(332, 0);
            this.radInquiriesComplex.Name = "radInquiriesComplex";
            this.radInquiriesComplex.Size = new System.Drawing.Size(31, 17);
            this.radInquiriesComplex.TabIndex = 36;
            this.radInquiriesComplex.Text = "6";
            this.radInquiriesComplex.UseVisualStyleBackColor = true;
            this.radInquiriesComplex.CheckedChanged += new System.EventHandler(this.updInquiries_ValueChanged);
            // 
            // radInquiriesAverage
            // 
            this.radInquiriesAverage.AutoSize = true;
            this.radInquiriesAverage.Checked = true;
            this.radInquiriesAverage.Location = new System.Drawing.Point(191, 0);
            this.radInquiriesAverage.Name = "radInquiriesAverage";
            this.radInquiriesAverage.Size = new System.Drawing.Size(31, 17);
            this.radInquiriesAverage.TabIndex = 36;
            this.radInquiriesAverage.TabStop = true;
            this.radInquiriesAverage.Text = "4";
            this.radInquiriesAverage.UseVisualStyleBackColor = true;
            this.radInquiriesAverage.CheckedChanged += new System.EventHandler(this.updInquiries_ValueChanged);
            // 
            // radInquiriesSimple
            // 
            this.radInquiriesSimple.AutoSize = true;
            this.radInquiriesSimple.Location = new System.Drawing.Point(50, 0);
            this.radInquiriesSimple.Name = "radInquiriesSimple";
            this.radInquiriesSimple.Size = new System.Drawing.Size(31, 17);
            this.radInquiriesSimple.TabIndex = 0;
            this.radInquiriesSimple.Text = "3";
            this.radInquiriesSimple.UseVisualStyleBackColor = true;
            this.radInquiriesSimple.CheckedChanged += new System.EventHandler(this.updInquiries_ValueChanged);
            // 
            // pnlInternalLogicRadio
            // 
            this.pnlInternalLogicRadio.Controls.Add(this.radLogicalComplex);
            this.pnlInternalLogicRadio.Controls.Add(this.radLogicalAverage);
            this.pnlInternalLogicRadio.Controls.Add(this.radLogicalSimple);
            this.pnlInternalLogicRadio.Location = new System.Drawing.Point(268, 232);
            this.pnlInternalLogicRadio.Name = "pnlInternalLogicRadio";
            this.pnlInternalLogicRadio.Size = new System.Drawing.Size(473, 20);
            this.pnlInternalLogicRadio.TabIndex = 38;
            // 
            // radLogicalComplex
            // 
            this.radLogicalComplex.AutoSize = true;
            this.radLogicalComplex.Location = new System.Drawing.Point(332, 0);
            this.radLogicalComplex.Name = "radLogicalComplex";
            this.radLogicalComplex.Size = new System.Drawing.Size(37, 17);
            this.radLogicalComplex.TabIndex = 36;
            this.radLogicalComplex.Text = "15";
            this.radLogicalComplex.UseVisualStyleBackColor = true;
            this.radLogicalComplex.CheckedChanged += new System.EventHandler(this.updLogical_ValueChanged);
            // 
            // radLogicalAverage
            // 
            this.radLogicalAverage.AutoSize = true;
            this.radLogicalAverage.Checked = true;
            this.radLogicalAverage.Location = new System.Drawing.Point(191, 0);
            this.radLogicalAverage.Name = "radLogicalAverage";
            this.radLogicalAverage.Size = new System.Drawing.Size(37, 17);
            this.radLogicalAverage.TabIndex = 36;
            this.radLogicalAverage.TabStop = true;
            this.radLogicalAverage.Text = "10";
            this.radLogicalAverage.UseVisualStyleBackColor = true;
            this.radLogicalAverage.CheckedChanged += new System.EventHandler(this.updLogical_ValueChanged);
            // 
            // radLogicalSimple
            // 
            this.radLogicalSimple.AutoSize = true;
            this.radLogicalSimple.Location = new System.Drawing.Point(50, 0);
            this.radLogicalSimple.Name = "radLogicalSimple";
            this.radLogicalSimple.Size = new System.Drawing.Size(31, 17);
            this.radLogicalSimple.TabIndex = 0;
            this.radLogicalSimple.Text = "7";
            this.radLogicalSimple.UseVisualStyleBackColor = true;
            this.radLogicalSimple.CheckedChanged += new System.EventHandler(this.updLogical_ValueChanged);
            // 
            // pnlExternalInterfaceRadio
            // 
            this.pnlExternalInterfaceRadio.Controls.Add(this.radInterfaceComplex);
            this.pnlExternalInterfaceRadio.Controls.Add(this.radInterfaceAverage);
            this.pnlExternalInterfaceRadio.Controls.Add(this.radInterfaceSimple);
            this.pnlExternalInterfaceRadio.Location = new System.Drawing.Point(268, 265);
            this.pnlExternalInterfaceRadio.Name = "pnlExternalInterfaceRadio";
            this.pnlExternalInterfaceRadio.Size = new System.Drawing.Size(473, 20);
            this.pnlExternalInterfaceRadio.TabIndex = 39;
            // 
            // radInterfaceComplex
            // 
            this.radInterfaceComplex.AutoSize = true;
            this.radInterfaceComplex.Location = new System.Drawing.Point(332, 0);
            this.radInterfaceComplex.Name = "radInterfaceComplex";
            this.radInterfaceComplex.Size = new System.Drawing.Size(37, 17);
            this.radInterfaceComplex.TabIndex = 36;
            this.radInterfaceComplex.Text = "10";
            this.radInterfaceComplex.UseVisualStyleBackColor = true;
            this.radInterfaceComplex.CheckedChanged += new System.EventHandler(this.updInterface_ValueChanged);
            // 
            // radInterfaceAverage
            // 
            this.radInterfaceAverage.AutoSize = true;
            this.radInterfaceAverage.Checked = true;
            this.radInterfaceAverage.Location = new System.Drawing.Point(191, 0);
            this.radInterfaceAverage.Name = "radInterfaceAverage";
            this.radInterfaceAverage.Size = new System.Drawing.Size(31, 17);
            this.radInterfaceAverage.TabIndex = 36;
            this.radInterfaceAverage.TabStop = true;
            this.radInterfaceAverage.Text = "7";
            this.radInterfaceAverage.UseVisualStyleBackColor = true;
            this.radInterfaceAverage.CheckedChanged += new System.EventHandler(this.updInterface_ValueChanged);
            // 
            // radInterfaceSimple
            // 
            this.radInterfaceSimple.AutoSize = true;
            this.radInterfaceSimple.Location = new System.Drawing.Point(50, 0);
            this.radInterfaceSimple.Name = "radInterfaceSimple";
            this.radInterfaceSimple.Size = new System.Drawing.Size(31, 17);
            this.radInterfaceSimple.TabIndex = 0;
            this.radInterfaceSimple.Text = "5";
            this.radInterfaceSimple.UseVisualStyleBackColor = true;
            this.radInterfaceSimple.CheckedChanged += new System.EventHandler(this.updInterface_ValueChanged);
            // 
            // txtInterfaceWithWeights
            // 
            this.txtInterfaceWithWeights.Location = new System.Drawing.Point(778, 267);
            this.txtInterfaceWithWeights.Name = "txtInterfaceWithWeights";
            this.txtInterfaceWithWeights.ReadOnly = true;
            this.txtInterfaceWithWeights.Size = new System.Drawing.Size(60, 20);
            this.txtInterfaceWithWeights.TabIndex = 44;
            this.txtInterfaceWithWeights.TextChanged += new System.EventHandler(this.calculateTotalCount);
            // 
            // txtLogicalWithWeights
            // 
            this.txtLogicalWithWeights.Location = new System.Drawing.Point(778, 234);
            this.txtLogicalWithWeights.Name = "txtLogicalWithWeights";
            this.txtLogicalWithWeights.ReadOnly = true;
            this.txtLogicalWithWeights.Size = new System.Drawing.Size(60, 20);
            this.txtLogicalWithWeights.TabIndex = 43;
            this.txtLogicalWithWeights.TextChanged += new System.EventHandler(this.calculateTotalCount);
            // 
            // txtInquiriesWithWeights
            // 
            this.txtInquiriesWithWeights.Location = new System.Drawing.Point(778, 201);
            this.txtInquiriesWithWeights.Name = "txtInquiriesWithWeights";
            this.txtInquiriesWithWeights.ReadOnly = true;
            this.txtInquiriesWithWeights.Size = new System.Drawing.Size(60, 20);
            this.txtInquiriesWithWeights.TabIndex = 42;
            this.txtInquiriesWithWeights.TextChanged += new System.EventHandler(this.calculateTotalCount);
            // 
            // txtExternalOutputsWithWeights
            // 
            this.txtExternalOutputsWithWeights.Location = new System.Drawing.Point(778, 168);
            this.txtExternalOutputsWithWeights.Name = "txtExternalOutputsWithWeights";
            this.txtExternalOutputsWithWeights.ReadOnly = true;
            this.txtExternalOutputsWithWeights.Size = new System.Drawing.Size(60, 20);
            this.txtExternalOutputsWithWeights.TabIndex = 41;
            this.txtExternalOutputsWithWeights.TextChanged += new System.EventHandler(this.calculateTotalCount);
            // 
            // txtExternalInputsWithWeights
            // 
            this.txtExternalInputsWithWeights.Location = new System.Drawing.Point(778, 133);
            this.txtExternalInputsWithWeights.Name = "txtExternalInputsWithWeights";
            this.txtExternalInputsWithWeights.ReadOnly = true;
            this.txtExternalInputsWithWeights.Size = new System.Drawing.Size(60, 20);
            this.txtExternalInputsWithWeights.TabIndex = 40;
            this.txtExternalInputsWithWeights.TextChanged += new System.EventHandler(this.calculateTotalCount);
            // 
            // txtTotalCount
            // 
            this.txtTotalCount.Location = new System.Drawing.Point(778, 320);
            this.txtTotalCount.Name = "txtTotalCount";
            this.txtTotalCount.ReadOnly = true;
            this.txtTotalCount.Size = new System.Drawing.Size(60, 20);
            this.txtTotalCount.TabIndex = 45;
            // 
            // btnComputeFP
            // 
            this.btnComputeFP.Location = new System.Drawing.Point(12, 365);
            this.btnComputeFP.Name = "btnComputeFP";
            this.btnComputeFP.Size = new System.Drawing.Size(120, 23);
            this.btnComputeFP.TabIndex = 46;
            this.btnComputeFP.Text = "Compute FP";
            this.btnComputeFP.UseVisualStyleBackColor = true;
            this.btnComputeFP.Click += new System.EventHandler(this.btnComputeFP_Click);
            // 
            // btnValueAdjustments
            // 
            this.btnValueAdjustments.Location = new System.Drawing.Point(12, 394);
            this.btnValueAdjustments.Name = "btnValueAdjustments";
            this.btnValueAdjustments.Size = new System.Drawing.Size(120, 23);
            this.btnValueAdjustments.TabIndex = 47;
            this.btnValueAdjustments.Text = "Value Adjustments";
            this.btnValueAdjustments.UseVisualStyleBackColor = true;
            this.btnValueAdjustments.Click += new System.EventHandler(this.btnValueAdjustments_Click);
            // 
            // btnComputerCodeSize
            // 
            this.btnComputerCodeSize.Location = new System.Drawing.Point(12, 423);
            this.btnComputerCodeSize.Name = "btnComputerCodeSize";
            this.btnComputerCodeSize.Size = new System.Drawing.Size(120, 23);
            this.btnComputerCodeSize.TabIndex = 48;
            this.btnComputerCodeSize.Text = "Compute Code Size";
            this.btnComputerCodeSize.UseVisualStyleBackColor = true;
            this.btnComputerCodeSize.Click += new System.EventHandler(this.btnComputerCodeSize_Click);
            // 
            // btnChangeLanguage
            // 
            this.btnChangeLanguage.Location = new System.Drawing.Point(12, 452);
            this.btnChangeLanguage.Name = "btnChangeLanguage";
            this.btnChangeLanguage.Size = new System.Drawing.Size(120, 23);
            this.btnChangeLanguage.TabIndex = 49;
            this.btnChangeLanguage.Text = "Change Language";
            this.btnChangeLanguage.UseVisualStyleBackColor = true;
            this.btnChangeLanguage.Click += new System.EventHandler(this.btnChangeLanguage_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(511, 427);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(126, 15);
            this.label10.TabIndex = 50;
            this.label10.Text = "Current Language:";
            // 
            // txtComputeFP
            // 
            this.txtComputeFP.Location = new System.Drawing.Point(778, 368);
            this.txtComputeFP.Name = "txtComputeFP";
            this.txtComputeFP.ReadOnly = true;
            this.txtComputeFP.Size = new System.Drawing.Size(60, 20);
            this.txtComputeFP.TabIndex = 51;
            // 
            // txtValueAdjustments
            // 
            this.txtValueAdjustments.Location = new System.Drawing.Point(778, 397);
            this.txtValueAdjustments.Name = "txtValueAdjustments";
            this.txtValueAdjustments.ReadOnly = true;
            this.txtValueAdjustments.Size = new System.Drawing.Size(60, 20);
            this.txtValueAdjustments.TabIndex = 52;
            // 
            // txtComputeCodeSize
            // 
            this.txtComputeCodeSize.Location = new System.Drawing.Point(747, 426);
            this.txtComputeCodeSize.Name = "txtComputeCodeSize";
            this.txtComputeCodeSize.ReadOnly = true;
            this.txtComputeCodeSize.Size = new System.Drawing.Size(91, 20);
            this.txtComputeCodeSize.TabIndex = 53;
            // 
            // txtLanguage
            // 
            this.txtLanguage.Location = new System.Drawing.Point(643, 426);
            this.txtLanguage.Name = "txtLanguage";
            this.txtLanguage.ReadOnly = true;
            this.txtLanguage.Size = new System.Drawing.Size(77, 20);
            this.txtLanguage.TabIndex = 54;
            // 
            // updInputs
            // 
            this.updInputs.Location = new System.Drawing.Point(153, 133);
            this.updInputs.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.updInputs.Name = "updInputs";
            this.updInputs.Size = new System.Drawing.Size(53, 20);
            this.updInputs.TabIndex = 55;
            this.updInputs.ValueChanged += new System.EventHandler(this.updInputs_ValueChanged);
            // 
            // updOutputs
            // 
            this.updOutputs.Location = new System.Drawing.Point(153, 166);
            this.updOutputs.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.updOutputs.Name = "updOutputs";
            this.updOutputs.Size = new System.Drawing.Size(53, 20);
            this.updOutputs.TabIndex = 56;
            this.updOutputs.ValueChanged += new System.EventHandler(this.updOutputs_ValueChanged);
            // 
            // updInquiries
            // 
            this.updInquiries.Location = new System.Drawing.Point(153, 199);
            this.updInquiries.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.updInquiries.Name = "updInquiries";
            this.updInquiries.Size = new System.Drawing.Size(53, 20);
            this.updInquiries.TabIndex = 57;
            this.updInquiries.ValueChanged += new System.EventHandler(this.updInquiries_ValueChanged);
            // 
            // updLogical
            // 
            this.updLogical.Location = new System.Drawing.Point(153, 232);
            this.updLogical.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.updLogical.Name = "updLogical";
            this.updLogical.Size = new System.Drawing.Size(53, 20);
            this.updLogical.TabIndex = 58;
            this.updLogical.ValueChanged += new System.EventHandler(this.updLogical_ValueChanged);
            // 
            // updInterface
            // 
            this.updInterface.Location = new System.Drawing.Point(153, 265);
            this.updInterface.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.updInterface.Name = "updInterface";
            this.updInterface.Size = new System.Drawing.Size(53, 20);
            this.updInterface.TabIndex = 59;
            this.updInterface.ValueChanged += new System.EventHandler(this.updInterface_ValueChanged);
            // 
            // FrmEnterFPData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(858, 547);
            this.Controls.Add(this.updInterface);
            this.Controls.Add(this.updLogical);
            this.Controls.Add(this.updInquiries);
            this.Controls.Add(this.updOutputs);
            this.Controls.Add(this.updInputs);
            this.Controls.Add(this.txtLanguage);
            this.Controls.Add(this.txtComputeCodeSize);
            this.Controls.Add(this.txtValueAdjustments);
            this.Controls.Add(this.txtComputeFP);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btnChangeLanguage);
            this.Controls.Add(this.btnComputerCodeSize);
            this.Controls.Add(this.btnValueAdjustments);
            this.Controls.Add(this.btnComputeFP);
            this.Controls.Add(this.txtTotalCount);
            this.Controls.Add(this.txtInterfaceWithWeights);
            this.Controls.Add(this.txtLogicalWithWeights);
            this.Controls.Add(this.txtInquiriesWithWeights);
            this.Controls.Add(this.txtExternalOutputsWithWeights);
            this.Controls.Add(this.txtExternalInputsWithWeights);
            this.Controls.Add(this.pnlExternalInterfaceRadio);
            this.Controls.Add(this.pnlInternalLogicRadio);
            this.Controls.Add(this.pnlExternalInquiriesRadio);
            this.Controls.Add(this.pnlExternalOutputsRadio);
            this.Controls.Add(this.pnlExternalInputsRadio);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblFunctionPointsTitle);
            this.Name = "FrmEnterFPData";
            this.Text = "Function Points";
            this.Load += new System.EventHandler(this.FrmEnterFPData_Load);
            this.pnlExternalInputsRadio.ResumeLayout(false);
            this.pnlExternalInputsRadio.PerformLayout();
            this.pnlExternalOutputsRadio.ResumeLayout(false);
            this.pnlExternalOutputsRadio.PerformLayout();
            this.pnlExternalInquiriesRadio.ResumeLayout(false);
            this.pnlExternalInquiriesRadio.PerformLayout();
            this.pnlInternalLogicRadio.ResumeLayout(false);
            this.pnlInternalLogicRadio.PerformLayout();
            this.pnlExternalInterfaceRadio.ResumeLayout(false);
            this.pnlExternalInterfaceRadio.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.updInputs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updOutputs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updInquiries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updLogical)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updInterface)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFunctionPointsTitle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel pnlExternalInputsRadio;
        private System.Windows.Forms.RadioButton radComplexInputs;
        private System.Windows.Forms.RadioButton radAvgInputs;
        private System.Windows.Forms.RadioButton radSimpleInputs;
        private System.Windows.Forms.Panel pnlExternalOutputsRadio;
        private System.Windows.Forms.RadioButton radOutputsComplex;
        private System.Windows.Forms.RadioButton radOutputsAverage;
        private System.Windows.Forms.RadioButton radOutputsSimple;
        private System.Windows.Forms.Panel pnlExternalInquiriesRadio;
        private System.Windows.Forms.RadioButton radInquiriesComplex;
        private System.Windows.Forms.RadioButton radInquiriesAverage;
        private System.Windows.Forms.RadioButton radInquiriesSimple;
        private System.Windows.Forms.Panel pnlInternalLogicRadio;
        private System.Windows.Forms.RadioButton radLogicalComplex;
        private System.Windows.Forms.RadioButton radLogicalAverage;
        private System.Windows.Forms.RadioButton radLogicalSimple;
        private System.Windows.Forms.Panel pnlExternalInterfaceRadio;
        private System.Windows.Forms.RadioButton radInterfaceComplex;
        private System.Windows.Forms.RadioButton radInterfaceAverage;
        private System.Windows.Forms.RadioButton radInterfaceSimple;
        private System.Windows.Forms.TextBox txtInterfaceWithWeights;
        private System.Windows.Forms.TextBox txtLogicalWithWeights;
        private System.Windows.Forms.TextBox txtInquiriesWithWeights;
        private System.Windows.Forms.TextBox txtExternalOutputsWithWeights;
        private System.Windows.Forms.TextBox txtExternalInputsWithWeights;
        private System.Windows.Forms.TextBox txtTotalCount;
        private System.Windows.Forms.Button btnComputeFP;
        private System.Windows.Forms.Button btnValueAdjustments;
        private System.Windows.Forms.Button btnComputerCodeSize;
        private System.Windows.Forms.Button btnChangeLanguage;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtComputeFP;
        private System.Windows.Forms.TextBox txtValueAdjustments;
        private System.Windows.Forms.TextBox txtComputeCodeSize;
        private System.Windows.Forms.TextBox txtLanguage;
        private System.Windows.Forms.NumericUpDown updInputs;
        private System.Windows.Forms.NumericUpDown updOutputs;
        private System.Windows.Forms.NumericUpDown updInquiries;
        private System.Windows.Forms.NumericUpDown updLogical;
        private System.Windows.Forms.NumericUpDown updInterface;
    }
}