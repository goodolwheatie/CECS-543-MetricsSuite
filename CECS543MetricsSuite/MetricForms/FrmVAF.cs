﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CECS543MetricsSuite
{
    public partial class FrmVAF : Form
    {
        List<int> cboSelectedIndices;
        public List<int> CboSelectedIndices
        {
            get { return CboSelectedIndices; }
            set { cboSelectedIndices = value; }
        }
        public FrmVAF()
        {
            InitializeComponent();
        }

        private void FrmVAF_Load(object sender, EventArgs e)
        {
            if (cboSelectedIndices == null)
            {
                cboSelectedIndices = new List<int>();
                foreach (ComboBox cb in pnlCboVAF.Controls)
                {
                    cb.SelectedIndex = 0;
                }
            }
            else
            {
                int index = 0;
                foreach (ComboBox cb in pnlCboVAF.Controls)
                {
                    cb.SelectedIndex = cboSelectedIndices[index];
                    ++index;
                }
            }
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            cboSelectedIndices = new List<int>();
            int totalVAF = 0;
            foreach (ComboBox cb in pnlCboVAF.Controls)
            {
                if (cb.SelectedIndex != -1)
                {
                    totalVAF += cb.SelectedIndex;
                }
                cboSelectedIndices.Add(cb.SelectedIndex);
            }
            cboSelectedIndices.Add(totalVAF); // append VAF to end of list to send to parent.
            this.Tag = cboSelectedIndices;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
