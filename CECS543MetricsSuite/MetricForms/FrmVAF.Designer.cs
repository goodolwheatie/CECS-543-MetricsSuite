﻿namespace CECS543MetricsSuite
{
    partial class FrmVAF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnDone = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cboBackup = new System.Windows.Forms.ComboBox();
            this.cboSpecialized = new System.Windows.Forms.ComboBox();
            this.cboDistributed = new System.Windows.Forms.ComboBox();
            this.cboCritical = new System.Windows.Forms.ComboBox();
            this.cboHeavily = new System.Windows.Forms.ComboBox();
            this.cboOnlineEntry = new System.Windows.Forms.ComboBox();
            this.cboMultiple = new System.Windows.Forms.ComboBox();
            this.cboUpdatedOnline = new System.Windows.Forms.ComboBox();
            this.cboInputOutputComplex = new System.Windows.Forms.ComboBox();
            this.cboInternalProcessingComplex = new System.Windows.Forms.ComboBox();
            this.cboReusable = new System.Windows.Forms.ComboBox();
            this.cboConversion = new System.Windows.Forms.ComboBox();
            this.cboDiffOrgan = new System.Windows.Forms.ComboBox();
            this.cboEaseofUse = new System.Windows.Forms.ComboBox();
            this.pnlCboVAF = new System.Windows.Forms.Panel();
            this.pnlCboVAF.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(593, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Assign a value from 0 to 5 for each of the following Value Adjustment Factors:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(351, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Does the system require reliable backup and recovery process?";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(527, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Are specialized data communications required to transfer information to or from t" +
    "he application?";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(238, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Are there distributed processing functions?";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 146);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(134, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Is performance critical?";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 176);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(415, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "Will the system run in an exisiting, heavily utilized operational environment?";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 206);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(239, 15);
            this.label7.TabIndex = 6;
            this.label7.Text = "Does the system require online data entry?";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 236);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(561, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = "Does the online data entry require the input transaction to be built over multipl" +
    "e screens or operations?";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 266);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(245, 15);
            this.label9.TabIndex = 8;
            this.label9.Text = "Are the internal logical files updated online?";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(12, 296);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(267, 15);
            this.label10.TabIndex = 9;
            this.label10.Text = "Are the input, output, files, or inquiries complex?";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(12, 326);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(200, 15);
            this.label11.TabIndex = 10;
            this.label11.Text = "Is the internal processing complex?";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(12, 356);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(208, 15);
            this.label12.TabIndex = 11;
            this.label12.Text = "Is the code designed to be reusable?";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(12, 386);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(303, 15);
            this.label13.TabIndex = 12;
            this.label13.Text = "Are conversion and installation included in the design?";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(12, 416);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(407, 15);
            this.label14.TabIndex = 13;
            this.label14.Text = "Is the system designed for multiple installations in different organizations?";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(12, 446);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(434, 15);
            this.label15.TabIndex = 14;
            this.label15.Text = "Is the application designed to facilitate change and for ease of use by the user?" +
    "";
            // 
            // btnDone
            // 
            this.btnDone.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnDone.Location = new System.Drawing.Point(15, 485);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(75, 23);
            this.btnDone.TabIndex = 15;
            this.btnDone.Text = "Done";
            this.btnDone.UseVisualStyleBackColor = true;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(96, 485);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 16;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cboBackup
            // 
            this.cboBackup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBackup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBackup.DisplayMember = "0";
            this.cboBackup.FormattingEnabled = true;
            this.cboBackup.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cboBackup.Location = new System.Drawing.Point(18, 13);
            this.cboBackup.Name = "cboBackup";
            this.cboBackup.Size = new System.Drawing.Size(42, 21);
            this.cboBackup.TabIndex = 17;
            // 
            // cboSpecialized
            // 
            this.cboSpecialized.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboSpecialized.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboSpecialized.DisplayMember = "0";
            this.cboSpecialized.FormattingEnabled = true;
            this.cboSpecialized.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cboSpecialized.Location = new System.Drawing.Point(18, 43);
            this.cboSpecialized.Name = "cboSpecialized";
            this.cboSpecialized.Size = new System.Drawing.Size(42, 21);
            this.cboSpecialized.TabIndex = 18;
            // 
            // cboDistributed
            // 
            this.cboDistributed.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDistributed.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDistributed.DisplayMember = "0";
            this.cboDistributed.FormattingEnabled = true;
            this.cboDistributed.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cboDistributed.Location = new System.Drawing.Point(18, 73);
            this.cboDistributed.Name = "cboDistributed";
            this.cboDistributed.Size = new System.Drawing.Size(42, 21);
            this.cboDistributed.TabIndex = 19;
            // 
            // cboCritical
            // 
            this.cboCritical.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboCritical.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCritical.DisplayMember = "0";
            this.cboCritical.FormattingEnabled = true;
            this.cboCritical.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cboCritical.Location = new System.Drawing.Point(18, 103);
            this.cboCritical.Name = "cboCritical";
            this.cboCritical.Size = new System.Drawing.Size(42, 21);
            this.cboCritical.TabIndex = 20;
            // 
            // cboHeavily
            // 
            this.cboHeavily.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboHeavily.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboHeavily.DisplayMember = "0";
            this.cboHeavily.FormattingEnabled = true;
            this.cboHeavily.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cboHeavily.Location = new System.Drawing.Point(18, 133);
            this.cboHeavily.Name = "cboHeavily";
            this.cboHeavily.Size = new System.Drawing.Size(42, 21);
            this.cboHeavily.TabIndex = 21;
            // 
            // cboOnlineEntry
            // 
            this.cboOnlineEntry.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboOnlineEntry.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboOnlineEntry.DisplayMember = "0";
            this.cboOnlineEntry.FormattingEnabled = true;
            this.cboOnlineEntry.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cboOnlineEntry.Location = new System.Drawing.Point(18, 163);
            this.cboOnlineEntry.Name = "cboOnlineEntry";
            this.cboOnlineEntry.Size = new System.Drawing.Size(42, 21);
            this.cboOnlineEntry.TabIndex = 22;
            // 
            // cboMultiple
            // 
            this.cboMultiple.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboMultiple.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboMultiple.DisplayMember = "0";
            this.cboMultiple.FormattingEnabled = true;
            this.cboMultiple.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cboMultiple.Location = new System.Drawing.Point(18, 193);
            this.cboMultiple.Name = "cboMultiple";
            this.cboMultiple.Size = new System.Drawing.Size(42, 21);
            this.cboMultiple.TabIndex = 23;
            // 
            // cboUpdatedOnline
            // 
            this.cboUpdatedOnline.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboUpdatedOnline.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboUpdatedOnline.DisplayMember = "0";
            this.cboUpdatedOnline.FormattingEnabled = true;
            this.cboUpdatedOnline.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cboUpdatedOnline.Location = new System.Drawing.Point(18, 223);
            this.cboUpdatedOnline.Name = "cboUpdatedOnline";
            this.cboUpdatedOnline.Size = new System.Drawing.Size(42, 21);
            this.cboUpdatedOnline.TabIndex = 24;
            // 
            // cboInputOutputComplex
            // 
            this.cboInputOutputComplex.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboInputOutputComplex.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboInputOutputComplex.DisplayMember = "0";
            this.cboInputOutputComplex.FormattingEnabled = true;
            this.cboInputOutputComplex.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cboInputOutputComplex.Location = new System.Drawing.Point(18, 253);
            this.cboInputOutputComplex.Name = "cboInputOutputComplex";
            this.cboInputOutputComplex.Size = new System.Drawing.Size(42, 21);
            this.cboInputOutputComplex.TabIndex = 25;
            // 
            // cboInternalProcessingComplex
            // 
            this.cboInternalProcessingComplex.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboInternalProcessingComplex.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboInternalProcessingComplex.DisplayMember = "0";
            this.cboInternalProcessingComplex.FormattingEnabled = true;
            this.cboInternalProcessingComplex.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cboInternalProcessingComplex.Location = new System.Drawing.Point(18, 283);
            this.cboInternalProcessingComplex.Name = "cboInternalProcessingComplex";
            this.cboInternalProcessingComplex.Size = new System.Drawing.Size(42, 21);
            this.cboInternalProcessingComplex.TabIndex = 26;
            // 
            // cboReusable
            // 
            this.cboReusable.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboReusable.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboReusable.DisplayMember = "0";
            this.cboReusable.FormattingEnabled = true;
            this.cboReusable.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cboReusable.Location = new System.Drawing.Point(18, 313);
            this.cboReusable.Name = "cboReusable";
            this.cboReusable.Size = new System.Drawing.Size(42, 21);
            this.cboReusable.TabIndex = 27;
            // 
            // cboConversion
            // 
            this.cboConversion.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboConversion.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboConversion.DisplayMember = "0";
            this.cboConversion.FormattingEnabled = true;
            this.cboConversion.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cboConversion.Location = new System.Drawing.Point(18, 343);
            this.cboConversion.Name = "cboConversion";
            this.cboConversion.Size = new System.Drawing.Size(42, 21);
            this.cboConversion.TabIndex = 28;
            // 
            // cboDiffOrgan
            // 
            this.cboDiffOrgan.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDiffOrgan.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDiffOrgan.DisplayMember = "0";
            this.cboDiffOrgan.FormattingEnabled = true;
            this.cboDiffOrgan.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cboDiffOrgan.Location = new System.Drawing.Point(18, 373);
            this.cboDiffOrgan.Name = "cboDiffOrgan";
            this.cboDiffOrgan.Size = new System.Drawing.Size(42, 21);
            this.cboDiffOrgan.TabIndex = 29;
            // 
            // cboEaseofUse
            // 
            this.cboEaseofUse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboEaseofUse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEaseofUse.DisplayMember = "0";
            this.cboEaseofUse.FormattingEnabled = true;
            this.cboEaseofUse.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cboEaseofUse.Location = new System.Drawing.Point(18, 403);
            this.cboEaseofUse.Name = "cboEaseofUse";
            this.cboEaseofUse.Size = new System.Drawing.Size(42, 21);
            this.cboEaseofUse.TabIndex = 30;
            // 
            // pnlCboVAF
            // 
            this.pnlCboVAF.Controls.Add(this.cboEaseofUse);
            this.pnlCboVAF.Controls.Add(this.cboDiffOrgan);
            this.pnlCboVAF.Controls.Add(this.cboConversion);
            this.pnlCboVAF.Controls.Add(this.cboReusable);
            this.pnlCboVAF.Controls.Add(this.cboInternalProcessingComplex);
            this.pnlCboVAF.Controls.Add(this.cboInputOutputComplex);
            this.pnlCboVAF.Controls.Add(this.cboUpdatedOnline);
            this.pnlCboVAF.Controls.Add(this.cboMultiple);
            this.pnlCboVAF.Controls.Add(this.cboOnlineEntry);
            this.pnlCboVAF.Controls.Add(this.cboHeavily);
            this.pnlCboVAF.Controls.Add(this.cboCritical);
            this.pnlCboVAF.Controls.Add(this.cboDistributed);
            this.pnlCboVAF.Controls.Add(this.cboSpecialized);
            this.pnlCboVAF.Controls.Add(this.cboBackup);
            this.pnlCboVAF.Location = new System.Drawing.Point(652, 37);
            this.pnlCboVAF.Name = "pnlCboVAF";
            this.pnlCboVAF.Size = new System.Drawing.Size(72, 448);
            this.pnlCboVAF.TabIndex = 31;
            // 
            // FrmVAF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 548);
            this.Controls.Add(this.pnlCboVAF);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmVAF";
            this.Text = "Value Adjustment Factors";
            this.Load += new System.EventHandler(this.FrmVAF_Load);
            this.pnlCboVAF.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox cboBackup;
        private System.Windows.Forms.ComboBox cboSpecialized;
        private System.Windows.Forms.ComboBox cboDistributed;
        private System.Windows.Forms.ComboBox cboCritical;
        private System.Windows.Forms.ComboBox cboHeavily;
        private System.Windows.Forms.ComboBox cboOnlineEntry;
        private System.Windows.Forms.ComboBox cboMultiple;
        private System.Windows.Forms.ComboBox cboUpdatedOnline;
        private System.Windows.Forms.ComboBox cboInputOutputComplex;
        private System.Windows.Forms.ComboBox cboInternalProcessingComplex;
        private System.Windows.Forms.ComboBox cboReusable;
        private System.Windows.Forms.ComboBox cboConversion;
        private System.Windows.Forms.ComboBox cboDiffOrgan;
        private System.Windows.Forms.ComboBox cboEaseofUse;
        private System.Windows.Forms.Panel pnlCboVAF;
    }
}