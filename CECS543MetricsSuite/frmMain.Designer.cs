﻿namespace CECS543MetricsSuite
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mnuMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.languagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.metricsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.functionPoointsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enterFPDataToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.softwareMaturityIndexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enterSMIDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projectCodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addCodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projectCodeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tabsMetrics = new System.Windows.Forms.TabControl();
            this.tvwMetrics = new System.Windows.Forms.TreeView();
            this.sptCtnMain = new System.Windows.Forms.SplitContainer();
            this.ctxtMnuTvwMetrics = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuClose = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sptCtnMain)).BeginInit();
            this.sptCtnMain.Panel1.SuspendLayout();
            this.sptCtnMain.Panel2.SuspendLayout();
            this.sptCtnMain.SuspendLayout();
            this.ctxtMnuTvwMetrics.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnuMain
            // 
            this.mnuMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.mnuMain.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.mnuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.preferencesToolStripMenuItem,
            this.metricsToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.projectCodeToolStripMenuItem});
            this.mnuMain.Location = new System.Drawing.Point(0, 0);
            this.mnuMain.Name = "mnuMain";
            this.mnuMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.mnuMain.Size = new System.Drawing.Size(1091, 24);
            this.mnuMain.TabIndex = 0;
            this.mnuMain.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Enabled = false;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Enabled = false;
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // preferencesToolStripMenuItem
            // 
            this.preferencesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.languagesToolStripMenuItem});
            this.preferencesToolStripMenuItem.Enabled = false;
            this.preferencesToolStripMenuItem.Name = "preferencesToolStripMenuItem";
            this.preferencesToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.preferencesToolStripMenuItem.Text = "Preferences";
            // 
            // languagesToolStripMenuItem
            // 
            this.languagesToolStripMenuItem.Name = "languagesToolStripMenuItem";
            this.languagesToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.languagesToolStripMenuItem.Text = "Languages";
            this.languagesToolStripMenuItem.Click += new System.EventHandler(this.languagesToolStripMenuItem_Click);
            // 
            // metricsToolStripMenuItem
            // 
            this.metricsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.functionPoointsToolStripMenuItem,
            this.softwareMaturityIndexToolStripMenuItem});
            this.metricsToolStripMenuItem.Enabled = false;
            this.metricsToolStripMenuItem.Name = "metricsToolStripMenuItem";
            this.metricsToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.metricsToolStripMenuItem.Text = "Metrics";
            // 
            // functionPoointsToolStripMenuItem
            // 
            this.functionPoointsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enterFPDataToolStripMenuItem1});
            this.functionPoointsToolStripMenuItem.Name = "functionPoointsToolStripMenuItem";
            this.functionPoointsToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.functionPoointsToolStripMenuItem.Text = "Function Points";
            // 
            // enterFPDataToolStripMenuItem1
            // 
            this.enterFPDataToolStripMenuItem1.Name = "enterFPDataToolStripMenuItem1";
            this.enterFPDataToolStripMenuItem1.Size = new System.Drawing.Size(144, 22);
            this.enterFPDataToolStripMenuItem1.Text = "Enter FP Data";
            this.enterFPDataToolStripMenuItem1.Click += new System.EventHandler(this.enterFPDataToolStripMenuItem1_Click);
            // 
            // softwareMaturityIndexToolStripMenuItem
            // 
            this.softwareMaturityIndexToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enterSMIDataToolStripMenuItem});
            this.softwareMaturityIndexToolStripMenuItem.Name = "softwareMaturityIndexToolStripMenuItem";
            this.softwareMaturityIndexToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.softwareMaturityIndexToolStripMenuItem.Text = "Software Maturity Index";
            // 
            // enterSMIDataToolStripMenuItem
            // 
            this.enterSMIDataToolStripMenuItem.Name = "enterSMIDataToolStripMenuItem";
            this.enterSMIDataToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.enterSMIDataToolStripMenuItem.Text = "Enter SMI Data";
            this.enterSMIDataToolStripMenuItem.Click += new System.EventHandler(this.enterSMIDataToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // projectCodeToolStripMenuItem
            // 
            this.projectCodeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addCodeToolStripMenuItem,
            this.projectCodeToolStripMenuItem1});
            this.projectCodeToolStripMenuItem.Enabled = false;
            this.projectCodeToolStripMenuItem.Name = "projectCodeToolStripMenuItem";
            this.projectCodeToolStripMenuItem.Size = new System.Drawing.Size(87, 20);
            this.projectCodeToolStripMenuItem.Text = "Project Code";
            // 
            // addCodeToolStripMenuItem
            // 
            this.addCodeToolStripMenuItem.Name = "addCodeToolStripMenuItem";
            this.addCodeToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.addCodeToolStripMenuItem.Text = "Add code";
            this.addCodeToolStripMenuItem.Click += new System.EventHandler(this.addCodeToolStripMenuItem_Click);
            // 
            // projectCodeToolStripMenuItem1
            // 
            this.projectCodeToolStripMenuItem1.Enabled = false;
            this.projectCodeToolStripMenuItem1.Name = "projectCodeToolStripMenuItem1";
            this.projectCodeToolStripMenuItem1.Size = new System.Drawing.Size(188, 22);
            this.projectCodeToolStripMenuItem1.Text = "Project code statistics";
            this.projectCodeToolStripMenuItem1.Click += new System.EventHandler(this.projectCodeToolStripMenuItem1_Click);
            // 
            // tabsMetrics
            // 
            this.tabsMetrics.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabsMetrics.Location = new System.Drawing.Point(0, 0);
            this.tabsMetrics.Multiline = true;
            this.tabsMetrics.Name = "tabsMetrics";
            this.tabsMetrics.SelectedIndex = 0;
            this.tabsMetrics.Size = new System.Drawing.Size(884, 544);
            this.tabsMetrics.TabIndex = 2;
            this.tabsMetrics.SelectedIndexChanged += new System.EventHandler(this.tabsMetrics_SelectedIndexChanged);
            // 
            // tvwMetrics
            // 
            this.tvwMetrics.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvwMetrics.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvwMetrics.Location = new System.Drawing.Point(0, 0);
            this.tvwMetrics.Name = "tvwMetrics";
            this.tvwMetrics.Size = new System.Drawing.Size(197, 544);
            this.tvwMetrics.TabIndex = 4;
            this.tvwMetrics.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvwMetrics_NodeMouseClick);
            this.tvwMetrics.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tvwMetrics_MouseUp);
            // 
            // sptCtnMain
            // 
            this.sptCtnMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sptCtnMain.Location = new System.Drawing.Point(0, 24);
            this.sptCtnMain.Margin = new System.Windows.Forms.Padding(0);
            this.sptCtnMain.Name = "sptCtnMain";
            // 
            // sptCtnMain.Panel1
            // 
            this.sptCtnMain.Panel1.Controls.Add(this.tvwMetrics);
            // 
            // sptCtnMain.Panel2
            // 
            this.sptCtnMain.Panel2.Controls.Add(this.tabsMetrics);
            this.sptCtnMain.Size = new System.Drawing.Size(1091, 544);
            this.sptCtnMain.SplitterDistance = 197;
            this.sptCtnMain.SplitterWidth = 10;
            this.sptCtnMain.TabIndex = 5;
            // 
            // ctxtMnuTvwMetrics
            // 
            this.ctxtMnuTvwMetrics.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuOpen,
            this.mnuClose,
            this.toolStripSeparator1,
            this.mnuDelete});
            this.ctxtMnuTvwMetrics.Name = "ctxtMnuTvwMetrics";
            this.ctxtMnuTvwMetrics.Size = new System.Drawing.Size(108, 76);
            // 
            // mnuOpen
            // 
            this.mnuOpen.Name = "mnuOpen";
            this.mnuOpen.Size = new System.Drawing.Size(180, 22);
            this.mnuOpen.Text = "Open";
            // 
            // mnuClose
            // 
            this.mnuClose.Name = "mnuClose";
            this.mnuClose.Size = new System.Drawing.Size(180, 22);
            this.mnuClose.Text = "Close";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(177, 6);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Size = new System.Drawing.Size(107, 22);
            this.mnuDelete.Text = "Delete";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1091, 568);
            this.Controls.Add(this.sptCtnMain);
            this.Controls.Add(this.mnuMain);
            this.MainMenuStrip = this.mnuMain;
            this.Name = "FrmMain";
            this.Text = "CECS 543 Metrics Suite";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.mnuMain.ResumeLayout(false);
            this.mnuMain.PerformLayout();
            this.sptCtnMain.Panel1.ResumeLayout(false);
            this.sptCtnMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sptCtnMain)).EndInit();
            this.sptCtnMain.ResumeLayout(false);
            this.ctxtMnuTvwMetrics.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnuMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preferencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem metricsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem languagesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem functionPoointsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enterFPDataToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem softwareMaturityIndexToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enterSMIDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projectCodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addCodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projectCodeToolStripMenuItem1;
        private System.Windows.Forms.TabControl tabsMetrics;
        private System.Windows.Forms.TreeView tvwMetrics;
        private System.Windows.Forms.SplitContainer sptCtnMain;
        private System.Windows.Forms.ContextMenuStrip ctxtMnuTvwMetrics;
        private System.Windows.Forms.ToolStripMenuItem mnuOpen;
        private System.Windows.Forms.ToolStripMenuItem mnuClose;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem mnuDelete;
    }
}

